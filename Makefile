# To be able to compile, install OCaml 4.01.0 and opam. Then do
#
# yes | opam init
# eval `opam config env`
# opam install batteries ocp-build ocamlnet

HL_ASSEMBLE=_obuild/hlassemble/hlassemble.asm
ASSEMBLE=_obuild/assemble/assemble.asm
GHC_ASSEMBLE = _obuild/ghcassemble/ghcassemble.asm

HLLs = \
  lambdaman/go-right.hll \
  lambdaman/somewhere_togo.hll \
  lambdaman/semi-random-ai.hll \
  lambdaman/strat_man.hll \
  lambdaman/strat_man_no_fruit.hll \
  lambdaman/strat_man_avoid1.hll \
  lambdaman/strat_man_avoid50.hll \
  lambdaman/strat_man_avoid_d6.hll \

GCCs=$(HLLs:%.hll=%.gcc)

.PRECIOUS:%.egcc

EGHCs = \
  ghc/ai/four-weights.eghc \
  ghc/ai/go-to-lambda.eghc \
  ghc/ai/default.eghc \

GHCs=$(EGHCs:%.eghc=%.ghc)

.PHONY: all
all: build $(GCCs) $(EGHCs)

build: ocp-build.root 
	ocp-build

.PHONY: clean
clean:
	ocp-build clean
	rm -rf $(HLLs:%.hll=%.egcc)
	rm -rf $(GCCs) $(GHCs)

.PHONY: test
test: all
	_obuild/tests/tests.asm -runner sequential

.PHONY: archive
archive: the_blind_hen.tar.gz

%.gcc: build %.egcc
	$(ASSEMBLE) $*.egcc > $@

%.egcc: build %.hll
	cpp $*.hll | $(HL_ASSEMBLE) > $*.egcc

%.ghc: build %.eghc
	cpp $*.eghc | $(GHC_ASSEMBLE) > $*.ghc

the_blind_hen.tar.gz: all .git/refs/heads/master
	rm -rf archive $@
	mkdir -p archive/solution
	mkdir -p archive/code
	cp lambdaman/strat_man_avoid_d6.gcc archive/solution/lambdaman.gcc
	cp ghc/ai/four-weights.ghc archive/solution/ghost0.ghc
	#cp ghc/ai/four-weights.eghc archive/solution/ghost1.gcc
	#cp ghc/ai/four-weights.eghc archive/solution/ghost2.gcc
	#cp ghc/ai/four-weights.eghc archive/solution/ghost3.gcc
	git archive HEAD |tar -C archive/code -x
	tar -C archive -czf $@ .
	rm -rf archive
	sha1sum $@

ocp-build.root:
	ocp-build root
	ocp-build configure -asm -byte -digest
