Lambdaman strategy: go in a direction that has good stuff if possible.
Otherwise, go in some possible pseudo-random direction, preferring our current
direction.

Ghost strategy: evaluate our four possible moves and assign them weights based
on several factors.
- Move toward Lambdaman in the coordinate where we're farthest from him
- Move away from him instead if he can reach us before his power pill expires
- Avoid other ghosts
- Don't revisit a field where we've been in the last eight steps
- Add a touch of randomness
- Don't make illegal moves

Written in OCaml. There's an assembly language with labels on top of GCC
(EGCC) and one on top of GHC (EGHC). On top of EGCC we have a high-level
language of ml/scheme flavour (Sweet). All languages are sent through the C
preprocessor to give us includes and macros.
