# Run this script on a fresh EC2 Ubuntu instance. SSH in as follows:
# $ ssh ubuntu@BLAH.BLAH.amazonaws.com < aws.sh

sudo apt-get update
sudo apt-get -y install git opam autoconf aspcud libffi-dev
sudo apt-get -y install vim-nox emacs24-nox zsh

git config --global color.ui auto
git config --global push.default simple
git config --global core.excludesfile \~/.gitexclude
git config --global core.pager 'less -R'

git config --global alias.st status
git config --global alias.ci commit
git config --global alias.co checkout

sudo tee -a /etc/profile >/dev/null <<EOF

. ~/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
EOF

sudo tee /etc/sudoers.d/50-blindhen >/dev/null <<EOF
%sudo ALL=(ALL) NOPASSWD:ALL
EOF

cat > .zshrc <<EOF
export EDITOR=vim VISUAL=vim
setopt nobgnice
PROMPT=$'%{\e[34m%}%3~ :%(?.%).()%{\e[0m%} '
bindkey -v
bindkey "^R" history-incremental-search-backward
bindkey "^W" backward-kill-word
bindkey "^?" backward-delete-char
export WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'
alias ls="ls --color=auto"
alias ll="ls -lh"
. ~/.opam/opam-init/init.sh
EOF

cat > .vimrc <<EOF
set et sw=2 sts=2 tw=80
set nobackup scrolloff=3
EOF

for u in crn jbj jsrn kaspersv; do
  sudo useradd -m -G sudo $u
  sudo su - $u <<EOF
  ln -s ~ubuntu/{.opam,.ocamlinit,.vimrc,.zshrc} .
  cp ~ubuntu/.gitconfig .
  mkdir .ssh
  touch .ssh/authorized_keys
  chmod 600 .ssh/authorized_keys
EOF
done

# Jonas
sudo usermod -s /bin/zsh jbj
sudo su - jbj <<EOF
git config --global user.name 'Jonas Jensen'
git config --global user.email 'jbj@knef.dk'
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSp759JrElzbCpbC/r3UOmg0I9eXpAZAavsA4rZBtLAM6D0YlbtWBoHu5U2KaZb2Y5cGpT8vBfwsXmES+mK5/u7V0f2gNbtXwtKXVPo2F5FDGDer7wZVwNiSJni1XfE2tzr2i+BvyZYoxtIpWjDQTgvlJrXjJokSGQk6iY9IzJnh+ugyY082aJy18tJ3SaFA0kAq1rllH9cFnT+Rn/qQAQUqj7I5eNjq6/yD8KtY/i1Y2lBqhMBtP5m+2hDqBydMfRttKbzl0SMeh6AaZMylOnp19x3yYMJUG0RflFEY2l6BfOpLl6/3lhckMxIG3AOTeNXWB3PUyBgiEhhFVfRAez jbj@tp >> .ssh/authorized_keys
EOF

# Johan
sudo su - jsrn <<EOF
git config --global user.name 'Johan S. R. Nielsen'
git config --global user.email 'jsrn@jsrn.dk'
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCuC4PCdq4tYNP+Stqa66QmfoeiC7amA+vnA8GntZuQd/fdCKuvbMciANhGN+aMHADU7Aw03evPXGc7/zmKPgNtiAv+zVsNoCMBP5AfokuBE2DeHPBLFYQ6VMHPVqLdtw+KY2/VDfgSwWdAe8/GSU6bZ/nS+BFgEoOPc7TXlDVQge4YRTi+8wu/tabvu4kbH6zgStWHcHxRFr1r+YBLf8JmZHHoGmDM8FFbOuGTC11Ag5B5262yOzCjZsZ7HDrtL2vXCwD17Rit95R4IESYuQ0OZysjKacnXSPrzGCef2MmV8u9+X1W3umoolKILJboFg+I6SRM/VsptziYQ7A9bELX jsrn@mole >> .ssh/authorized_keys
EOF

# Kasper
sudo su - kaspersv <<EOF
git config --global user.name 'Kasper Svendsen'
git config --global user.email 'kaspersv@gmail.com'
echo ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAvjR6uLv+7d3FsSQkcOtk/3GEvr5U5Pw/SLtbLOwU+WWQ/qaGdSV/Y+leO6MPH4Lo9Eaj+d24fAyaDJmtE3ywtd0bJYIvuwGKQDlLIXC1wBoGW9q21XZ0MHE85745VTLGAk671GzPQ2bLzzabj/lW3StRJR1KV4PUs6waULbRj7scGTbMXbMyH7ybvrv+V5bt2QeYHmZqt6knCItmMeW/upIzFcRtokZkUGlPhmx8iy/RTOsizl05BbphRkGb+GuzWJ6uFHUOIPXLhJwnBjTopiMkk6OvYpIpKTyULCBwrKOd2HUoSNq4BrsyLMN9MFUqP2XeGWPaskyn0m5UPCG3Ow== kaspersv@kaspersv-laptop >> .ssh/authorized_keys
EOF

# Christoffer
sudo su - crn <<EOF
git config --global user.name 'Christoffer R. Nielsen'
git config --global user.email 'chroni@live.dk'
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6SrlN5ZvuS9Y/isU8rcdfy8y/v8EhXaNRMiDQkOrsxfXtdQ48eGexmdPjTjNpPRLuH+FwwjX5S3xl36B4EN6vDngVKMPslZsgFgJKXr1DHGC2z/P5dEcKksJAcKrIki2Z2w+yI4QewbSTmnX5oImDhUDJdYB6NUY3qYDBRSI0W28eMK+JqmvQHN/K49IicYNqEpINMbq9UauFDd7JajuXHIfWlbXzD2LEnCAZNR6YkAUH4ROlyrPLgfc7qhefEh4uW+s/Jj3LjbaJigbdhfd+MlruAs5G94hRapFgo9QjOkcrXoQn/AIjeQAA3SMIq4afQUb5gt3pv8pY7MyFkH1N crn@crn >> .ssh/authorized_keys
EOF

# OPAM
yes | opam init
chmod 1777 .opam/log
eval `opam config env`

# The slow part
opam install -y ounit batteries ocp-build merlin ocamlnet ctypes
