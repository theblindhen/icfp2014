let cpp_string str =
    let b = Buffer.create (String.length str) in
    Shell.(call ~stdin:(from_string str) ~stdout:(to_buffer b)
                   [ cmd "cpp" [] ]);
    Buffer.contents b

  
let set_runtime_params () =
  (* Always record backtraces (though they might not be very good in native
   * code). It costs almost nothing to turn this on. *)
  Printexc.record_backtrace true;

  (* These parameters speed up OCaml by about 2x by sacrificing some RAM. See
   * https://elehack.net/writings/programming/ocaml-memory-tuning *)
  Gc.set { (Gc.get()) with
             Gc.minor_heap_size = 4*1024*1024;
             Gc.major_heap_increment = 32*1024*1024;
             Gc.space_overhead = 150; }
