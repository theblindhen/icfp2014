open Batteries

type label = string

type line =
  | I of label Gcc.inst
  | L of label

type prg = line list

let spr = Printf.sprintf

let line_to_str = function
  | I i -> "  " ^ (Gcc.inst_to_str (spr "%s") i)
  | L l -> spr "%s:" l

let prg_to_str prg =
  List.fold_left
    (fun acc line ->
       (if acc <> "" then acc ^ "\n" else "") ^ (line_to_str line))
    "" prg

let of_instrs (prg: label Gcc.inst list) : prg =
  List.map (fun i -> I i) prg

let compile (prg : prg) : Gcc.prg =
  let count = ref 0 in
  let lmap = Hashtbl.create 10 in
    List.iter
      (function
	| L l ->
            if Hashtbl.mem lmap l then
              failwith (Printf.sprintf "label %s declared more than once" l)
            else
              Hashtbl.add lmap l !count
        | I i -> count := !count + 1) prg;
    List.filter_map 
      (function 
	| L _ -> None
	| I i -> Some (Gcc.map_inst (fun l -> 
		   if Hashtbl.mem lmap l then 
		     Hashtbl.find lmap l 
		   else 
		     failwith (spr "label %s not declared" l)) i)) prg

let unittest () =
  let open OUnit2 in

  "EGCC" >::: [
    "Check printing" >::
      begin fun _ ->
        assert_equal ~printer:dump
          "a:\n  LDF b" (prg_to_str [L "a"; I (Gcc.LDF "b")])
      end;

    "Check compiling" >::
      begin fun _ ->
        assert_equal ~printer:dump
	  [Gcc.LDF 0] (compile [L "a"; I (Gcc.LDF "a")])
      end
  ]
