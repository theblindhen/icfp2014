open Batteries
open Sim

type cell =
  | Wall | Empty | Pill | PowerPill | Fruit | LambdaStart | GhostStart

type dir =
  | Left | Right | Up | Down

let int_to_dir dir =
  match dir with
  | 0 -> Up
  | 1 -> Right
  | 2 -> Down
  | 3 -> Left
  | _ -> failwith "invalid direction"

let dir_to_str = function
  | Up -> "up"
  | Right -> "right"
  | Down -> "down"
  | Left -> "left"

let dir_to_int = function
  | Up -> 0 
  | Right -> 1 
  | Down -> 2 
  | Left -> 3

type gh_vitality = | Std | Fright | Invisible
let int_of_gh_vitality = function | Std -> 0 | Fright -> 1 | Invisible -> 2
                          
type lmstatus = {
  mutable llocation  : int * int;
  mutable ldirection : dir;
  mutable lives      : int;
  mutable score      : int;
  mutable tick       : int;
  lstart_location : int * int;
}

type ghstatus = {
  mutable glocation  : int * int;
  mutable gdirection : dir;
  mutable tick       : int;
  mutable gvitality  : gh_vitality;
  mutable gnever_moved : bool;
  gstart_location : int * int;
}

type fstatus = int
type map_type = ((int * int), cell) Hashtbl.t

and world = { 
  map     : map_type;
  msize   : int * int;
  lman    : lmstatus;
  ghosts  : ghstatus list;
  mutable fruit   : fstatus;
  mutable fright  : fstatus;
  mutable eating_frenzy : fstatus;
  mutable ghost_ais: (world -> dir option) list;
  mutable lambda_ai: world -> dir;
  mutable utc : int;
  mutable pills_left : int;
  mutable game_won : bool;
  mutable game_over: bool;
}

let encode_cell = function
  | Wall -> 0
  | Empty -> 1
  | Pill -> 2
  | PowerPill -> 3
  | Fruit -> 4
  | LambdaStart -> 5
  | GhostStart -> 6

let build_map map_string =
  let map_lines = List.filter (fun line -> String.length line <> 0) (String.nsplit map_string "\n") in
  let _ =
    let n = String.length (List.hd map_lines) in
    List.iteri (fun i s ->
      if String.length s != n then
        failwith (Printf.sprintf "Line %d not of length %d, %d" i n (String.length s))
      else ()) map_lines in
  let map = Hashtbl.create 10 in
  List.iteri (fun i line -> 
    List.iteri (fun j c -> 
      let cell = 
	match c with
	| '#' -> Wall 
	| ' ' -> Empty 
	| '.' -> Pill 
	| 'o' -> PowerPill 
	| '%' -> Fruit 
	| '\\'-> LambdaStart 
	| '=' -> GhostStart
	| _ -> failwith (Printf.sprintf "unsupported map character %c" c)
      in Hashtbl.add map (j, i) cell) (String.explode line)) map_lines;
  map

let print_world world =
  let size = world.msize in
  let map_str = ref "" in
  for i = (snd size) - 1 downto 0 do
    if !map_str <> "" then map_str := "\n" ^ (!map_str) else ();
    for j = (fst size) - 1 downto 0 do
      let cell_str = 
	if (j, i) = world.lman.llocation then "\\"
        else
	  match List.index_of (j, i) (List.map (fun ghost -> ghost.glocation) world.ghosts) with
          | Some gi -> string_of_int gi
          | None ->
             (match Hashtbl.find world.map (j, i) with
             | Wall -> "#"
             | Empty -> " "
             | Pill -> "."
             | PowerPill -> "o"
             | Fruit -> "%"
             | _ -> " ")
      in map_str := cell_str ^ (!map_str)
    done;
  done;
  Printf.printf "\nTicks: %d\tScore: %d\tLives: %d\n%s\n" (1+world.utc) world.lman.score world.lman.lives (!map_str)




                
(*     Implement the Game Rules   *)
let opposite_dir = function
  | Down -> Up
  | Up -> Down
  | Left -> Right
  | Right -> Left

let pos_static_object world pos = Hashtbl.find world.map pos
let pos_is_free world pos = pos_static_object world pos <> Wall

let pos_after_move (x,y) = function
  | Down -> (x,y+1)
  | Left -> (x-1,y)
  | Right -> (x+1,y)
  | Up -> (x,y-1)

let pos_free_neighbors world pos =
  [ Left ; Up ; Down ; Right ]
  |> List.map (pos_after_move pos)
  |> List.filter (pos_is_free world)

let pos_free_neighbor_dirs world pos =
  [ Left ; Up ; Down ; Right ]
  |> List.filter (fun dir -> pos_is_free world (pos_after_move pos dir))

let is_junction world pos =
  List.length (pos_free_neighbors world pos) > 2

let is_deadend world pos =
  List.length (pos_free_neighbors world pos) = 1

let legal_ghost_move world ghost dir =
  let npos = pos_after_move ghost.glocation dir in
  (pos_is_free world npos) &&
     ((ghost.gdirection = dir)
     || ((is_junction world ghost.glocation) && (ghost.gdirection <> opposite_dir dir))
     || ghost.gnever_moved (*init*)
     || (is_deadend world ghost.glocation))

let score_of_yummi world = function
  | Pill -> 10
  | PowerPill -> 50
  | Fruit ->
     if world.fruit <= 0 then 0
     else
        (let level =
          let diff = fst world.msize * snd world.msize in
          diff / 100 + (if diff mod 100 = 0 then 0 else 1) in
        let level_scores = 
          [(1 ,100 ) ;(2 ,300 ) ;(3 ,500 ) ;(4 ,500 ) ;(5 ,700 ) ;(6 ,700)
           ;(7 ,1000) ;(8 ,1000) ;(9 ,2000) ;(10,2000) ;(11,3000) ;(12,3000) ]
        in
        try 
          snd (List.find (fun (l,_) -> l=level) level_scores)
        with Not_found -> (* level>12 *)
             5000)
  | _ -> failwith "Doesn't have a score"

let get_creature_start_pos map msize =
  let (lman_pos, ghs_pos_r) =
    Enum.fold (fun (lp,gp) y ->
      Enum.fold (fun (lp,gp) x ->
        match Hashtbl.find map (x,y) with
        | GhostStart  -> (lp, (x,y)::gp)
        | LambdaStart -> ((x,y), gp)
        | _ -> (lp,gp)
      ) (lp,gp) (0--(fst msize-1))
    ) ((-1,-1), []) (0--(snd msize-1)) in
  (lman_pos, List.rev ghs_pos_r)

let fresh_world map =
  (* A world without the lambda ai *)
  let msize =
    let (mx,my) = Hashtbl.fold (fun (x,y) _ (mx,my) -> (max x mx, max y my)) map (0,0) in
    (mx+1,my+1) in
  let (lman_start_pos, ghs_start_pos) = get_creature_start_pos map msize in
  let lman = {
    llocation = lman_start_pos;
    ldirection = Down;
    lives = 3;
    score = 0;
    tick = 127;
    lstart_location = lman_start_pos;
  } in
  let ghosts = List.mapi (fun i gh_start_pos -> {
    glocation = gh_start_pos;
    gdirection = Down;
    tick = 130 + 2*(i mod 4);
    gvitality = Std;
    gstart_location = gh_start_pos;
    gnever_moved = true;
  }) ghs_start_pos in
  {
    map = map;
    msize = msize;
    lman = lman;
    ghosts = ghosts;
    fruit = 0;
    fright = 0;
    eating_frenzy = 0;
    ghost_ais = [];
    lambda_ai = (fun _ -> failwith "Uninitialized lambda ai");
    utc = 0;
    pills_left = map |> Hashtbl.enum |> Enum.filter (fun (_,o) -> o=Pill) |> Enum.count;
    game_won = false;
    game_over = false;
  }

let reset_creatures world =
  (* Reset positions and directions of lambdaman and ghosts *)
  world.lman.llocation <- world.lman.lstart_location;
  world.lman.ldirection <- Down;
  List.iter (fun gh ->
    gh.glocation <- gh.gstart_location;
    gh.gdirection <- Down) world.ghosts

let time_to_fruit utc = 
  if utc < 127 * 200 then
    127*200 - utc
  else if utc < 127 * 400 then
    127*400 - utc
  else
    Int.max_num

let eol world = 127 * fst world.msize * snd world.msize * 16

(* Run ticks in the world until next event *)
let run_one_event world =
  if world.game_over then
    failwith "Game already over. Stop playing!";
  (* Determine ticks taken all update all counters *)
  let diff_tick =
     (world.lman.tick
    ::world.fruit
    ::world.fright
    ::time_to_fruit world.utc
    ::(eol world-world.utc)
    ::List.map (fun gh -> gh.tick) world.ghosts)
    |> List.filter (fun x -> x > 0)
    |> List.reduce min 
  in
  world.lman.tick <- world.lman.tick - diff_tick;
  world.fruit <- world.fruit - diff_tick;
  (* fright decreased later *)
  List.iter (fun gh -> gh.tick <- gh.tick-diff_tick) world.ghosts;

  world.utc <- world.utc + diff_tick;


  (* Move lambdaman if tick is zero. His ticks are reset when determining eating *) 
  if world.lman.tick = 0  then (
    let dir = world.lambda_ai world in
    let new_pos = pos_after_move world.lman.llocation dir in
    if pos_is_free world new_pos then (
      world.lman.llocation <- new_pos;
      world.lman.ldirection <- dir; (* HMM, should we also set dir when move did not succeed? *)
    )
  );
  (* Move ghosts whose ticks are zero *)
  List.iteri (fun i gh ->
    if gh.tick = 0 then (
      (* Ghosts must move *)
      gh.tick <-
        if world.fright>0 (* NOTE: Should this be checked AFTER eating a Power Pill this turn? *)
        then 195 + 3*(i mod 4)
        else 130 + 2*(i mod 4);
      let ai = List.nth world.ghost_ais i in
      (*TODO: This will crash if the ghost is captured by four walls *)
      let dir =
        let wish_dir = ai world in
        (*(match wish_dir with
	 | Some dir -> Printf.printf "Ghost %d wished %s at %s\n" i (dir_to_str dir) (dump gh.glocation)
	 | None -> Printf.printf "Ghost %d did not make a wish\n" i);*)
        (* ignore ai's wish if not at a junction *)
        if not (is_junction world gh.glocation)
                   (* corner case for inits*)
                && (pos_is_free world (pos_after_move gh.glocation (opposite_dir gh.gdirection))) then
          match pos_free_neighbor_dirs world gh.glocation with
          | a::b::[] ->
             if legal_ghost_move world gh gh.gdirection then
               gh.gdirection
             else 
               (if opposite_dir gh.gdirection = a then b else a)
          | a::[] -> a
          | _ -> failwith "It WAS a junction!"
        else
          (* Junction or an init *)
          if wish_dir <> None && (legal_ghost_move world gh (Option.get wish_dir)) then
             (* Printf.printf "Ghost %d wished %s at %s\n" i (dir_to_str dir) (dump gh.glocation); *)
            Option.get wish_dir 
          else
            (* When a ghost chooses an illegal move (or no move at all) at a junction,
               it is forced to continue in its previous direction if this is legal,
               and if not, then the first legal direction out of up, right, down, and
               left, in that order. *)
            (* Printf.printf "\t\twish DENIED\n"; *)
            (* cyclic
            let dirs = [ Up ; Right ; Down ; Left ] in
            let dir_ind = Option.get (List.index_of gh.gdirection dirs) in
            let cyc_dirs =
              let a,b = List.split_at dir_ind dirs in
              b@a in
             *)
            List.find (fun d -> pos_is_free world (pos_after_move gh.glocation d))
                      (List.remove [ gh.gdirection ; Up ; Right ; Down ; Left ] (opposite_dir gh.gdirection))
      in
      gh.glocation <- (pos_after_move gh.glocation dir);
      gh.gdirection <- dir;
      gh.gnever_moved <- false
    )
  ) world.ghosts;


  (* Fright on/off , Fruit on/off *)
  if world.utc = 127 * 200 || world.utc = 127*400 then
    world.fruit <- 127 * 80;
  if world.fright = diff_tick then (
    world.eating_frenzy <- 0;
    List.iter (fun gh -> gh.gvitality <- Std) world.ghosts
  );
  world.fright <- world.fright-diff_tick;

  (* Eating yummies and resetting ticks. Check only of lambdaman moved this event *)
  if world.lman.tick = 0 then (
    let sobj = pos_static_object world world.lman.llocation in
    match sobj with
    | Pill
    | PowerPill
    | Fruit ->
       Hashtbl.add world.map world.lman.llocation Empty;
       world.lman.score <- world.lman.score + score_of_yummi world sobj;
       if sobj = Pill then world.pills_left <- world.pills_left-1;
       if sobj = PowerPill then (
         world.fright <- 127*20;
         List.iter (fun gh -> gh.gdirection <- opposite_dir gh.gdirection;
                              if gh.gvitality <> Invisible then gh.gvitality <- Fright
                   ) world.ghosts;
       );
       world.lman.tick <- 137 (* eating *)
    | _ ->
       world.lman.tick <- 127 (* starving *)
  ) ;
  
  (* Eating or getting eaten by ghosts -- that is the question *)
  let colliding_ghosts =
    List.filter (fun gh -> gh.glocation = world.lman.llocation
                              && gh.gvitality <> Invisible) world.ghosts
  in
  if List.length colliding_ghosts > 0 then (
    if world.fright > 0 then (
      (* We eat! *)
      Printf.printf "We just ate a ghost!\n";
      List.iter (fun gh ->
          world.eating_frenzy <- world.eating_frenzy + 1;
          world.lman.score <- world.lman.score + 100*(Int.pow 2 (min 4 world.eating_frenzy));
          gh.glocation <- gh.gstart_location;
          gh.gnever_moved <- true;
          gh.gdirection <- Down;
          gh.gvitality <- Invisible;
        ) colliding_ghosts
    ) else (
      (* Get eaten *)
      world.lman.lives <- world.lman.lives-1;
      reset_creatures world
    )
  );

  (* Winning *)
  if world.pills_left = 0 then (
    world.lman.score <- world.lman.score * (world.lman.lives + 1);
    world.game_won  <- true;
    world.game_over <- true;
  );

  (* Losing *)
  if world.lman.lives = 0 || world.utc = eol world then (
    world.game_over <- true;
    world.game_won <- false;
  );

  0
