open Batteries

type literal = int

type 'a inst =
 | LDC of literal
 | LD of literal * literal
 | ADD
 | SUB
 | MUL
 | DIV
 | CEQ
 | CGT
 | CGTE
 | ATOM
 | CONS
 | CAR
 | CDR
 | SEL of 'a * 'a
 | JOIN
 | LDF of 'a
 | AP of literal
 | RTN
 | DUM of literal
 | RAP of literal
 | STOP
 | TSEL of 'a * 'a
 | TAP of literal
 | TRAP of literal
 | ST of literal * literal
 | DBUG
 | BRK

type prg = (literal inst) list

let spr = Printf.sprintf

let map_inst (map_a : 'a -> 'b) = function
  | SEL (t, f) -> SEL (map_a t, map_a f)
  | LDF f -> LDF (map_a f)
  | TSEL (t, f) -> TSEL (map_a t, map_a f)
  | LDC n -> LDC n
  | LD (n, i) -> LD (n, i)
  | ADD -> ADD
  | SUB -> SUB
  | MUL -> MUL
  | DIV -> DIV
  | CEQ -> CEQ
  | CGT -> CGT
  | CGTE -> CGTE
  | ATOM -> ATOM
  | CONS -> CONS
  | CAR -> CAR
  | CDR -> CDR
  | JOIN -> JOIN
  | AP n -> AP n
  | RTN -> RTN
  | DUM n -> DUM n
  | RAP n -> RAP n
  | STOP -> STOP
  | TAP n -> TAP n
  | TRAP n -> TRAP n
  | ST (n, i) -> ST (n, i)
  | DBUG -> DBUG
  | BRK -> BRK

let inst_to_str pr_a = function
  | LDC n -> spr "LDC %d" n
  | LD (n, i) -> spr "LD %d %d" n i
  | ADD -> spr "ADD"
  | SUB -> spr "SUB"
  | MUL -> spr "MUL"
  | DIV -> spr "DIV"
  | CEQ -> spr "CEQ"
  | CGT -> spr "CGT"
  | CGTE -> spr "CGTE"
  | ATOM -> spr "ATOM"
  | CONS -> spr "CONS"
  | CAR -> spr "CAR"
  | CDR -> spr "CDR"
  | SEL (t, f) -> spr "SEL %s %s" (pr_a t) (pr_a f)
  | JOIN -> spr "JOIN"
  | LDF f -> spr "LDF %s" (pr_a f)
  | AP n -> spr "AP %d" n
  | RTN -> spr "RTN"
  | DUM n -> spr "DUM %d" n
  | RAP n -> spr "RAP %d" n
  | STOP -> spr "STOP"
  | TSEL (t, f) -> spr "TSEL %s %s" (pr_a t) (pr_a f)
  | TAP n -> spr "TAP %d" n
  | TRAP n -> spr "TRAP %d" n
  | ST (n, i) -> spr "ST %d %d" n i
  | DBUG -> spr "DBUG"
  | BRK -> spr "BRK"

let prg_to_str prg =
  List.fold_left
    (fun acc inst ->
       (if acc <> "" then acc ^ "\n" else "") ^ (inst_to_str (spr "%d") inst))
    "" prg

let unittest () =
  let open OUnit2 in

  "prg_to_str" >::: [
    "Check output" >::
      begin fun _ ->
        assert_equal ~printer:dump
          "ADD\nSUB\nSEL 4 5" (prg_to_str [ADD; SUB; SEL (4, 5)])
      end
  ]
