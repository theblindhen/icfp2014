{
  open Parser
  open Lexing

  let pos lexbuf = (lexeme_start lexbuf, lexeme_end lexbuf)

  let advance_line_pos pos =
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum; }

  let advance_line lexbuf =
    lexbuf.lex_curr_p <- advance_line_pos lexbuf.lex_curr_p
                       
}
rule token = parse
  [' ' '\t'] { token lexbuf }
| [';'] [^'\n']* ['\n'] { advance_line lexbuf ; NL }
| ['-']? ['0'-'9']+ as lxm { INT(int_of_string lxm) }
| "LDC" { LDC }
| "LD" { LD }
| "ADD" { ADD }
| "SUB" { SUB }
| "MUL" { MUL }
| "DIV" { DIV }
| "CEQ" { CEQ }
| "CGT" { CGT }
| "CGTE" { CGTE }
| "ATOM" { ATOM }
| "CONS" { CONS }
| "CAR" { CAR }
| "CDR" { CDR }
| "SEL" { SEL }
| "JOIN" { JOIN }
| "LDF" { LDF }
| "AP" { AP }
| "RTN" { RTN }
| "DUM" { DUM }
| "RAP" { RAP }
| "STOP" { STOP }
| "TSEL" { TSEL }
| "TAP" { TAP }
| "TRAP" { TRAP }
| "ST" { ST }
| "DBUG" { DBUG }
| "BRK" { BRK }
| ':' { COLON }
| ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '_' '0'-'9']* as lxm { LABEL(lxm) }
| ['\n'] { advance_line lexbuf ; NL }
| eof { EOF }
