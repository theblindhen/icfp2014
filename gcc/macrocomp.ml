open Batteries

let _ =
  let lexbuf = Lexing.from_channel (open_in Sys.argv.(1)) in
  let result = Mparser.prog Mlexer.token lexbuf in
    Printf.printf "Parsed program:\n%s\n" (dump result);
    Printf.printf "Compiled program:\n%s\n" (Egcc.prg_to_str (Macros.compProg result))(*;
    Printf.printf "Assembled program:\n%s\n" (Gcc.prg_to_str (Egcc.compile (Macros.compProg result)))*)
