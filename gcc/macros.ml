open Batteries

type var = string
type bop = Add | Sub | Mul | Cons | Eq | Gt | Div
type uop = Fst | Snd | Atom

type expr =
  | Lit of int
  | Var of var
  | Unop of uop * expr
  | Binop of bop * expr * expr
  | Call of Egcc.label * expr list
  | If of expr * expr * expr
  | Let of var * expr * expr
  | Debug of expr * expr

type meth = Egcc.label * var list * expr
type prg = meth list

let compUni = function
  | Snd -> Gcc.CDR
  | Fst -> Gcc.CAR
  | Atom -> Gcc.ATOM

let compBin = function
  | Add -> Gcc.ADD
  | Sub -> Gcc.SUB
  | Mul -> Gcc.MUL
  | Div -> Gcc.DIV
  | Cons -> Gcc.CONS
  | Eq -> Gcc.CEQ
  | Gt -> Gcc.CGT

let rec compExp lbl vars last = function
  | Lit i -> [Egcc.I (Gcc.LDC i)] @ last
  | Var v -> 
     (match Hashtbl.find_option vars v with
      | Some (n, i) -> [Egcc.I (Gcc.LD (n, i))] @ last
      | None -> failwith (Printf.sprintf "variable %s not defined" v))
  | Unop (u, e) -> compExp lbl vars [] e @ [Egcc.I (compUni u)] @ last
  | Binop (b, e1, e2) -> 
      compExp (lbl ^ "_btrue") vars [] e1 @ compExp (lbl ^ "_bfalse") vars [] e2 @ [Egcc.I (compBin b)] @ last
  | Call (l, el) -> 
     List.flatten (List.mapi (fun i e -> compExp (lbl ^ (Printf.sprintf "_arg%d" i)) vars [] e) el) @
     [Egcc.I (Gcc.LDF l); Egcc.I (Gcc.AP (List.length el))] @ last
  | If (eb, e1, e2) ->
     compExp (lbl ^ "_branch") vars [] eb @ 
     [Egcc.I (Gcc.SEL (lbl ^ "_btrue", lbl ^ "_bfalse"))] @ last @
     [Egcc.L (lbl ^ "_btrue")] @ compExp (lbl ^ "_btrue") vars [Egcc.I Gcc.JOIN] e1 @ 
     [Egcc.L (lbl ^ "_bfalse")] @ compExp (lbl ^ "_bfalse") vars [Egcc.I Gcc.JOIN] e2
  | Let (var, e1, e2) -> 
     let vars' = Hashtbl.map (fun x (n, i) -> (n + 1, i)) vars in
     let lbl' = (lbl ^ "_let" ^ var) in
       Hashtbl.add vars' var (0, 0); 
       compExp (lbl ^ "_arg") vars [] e1 @ [Egcc.I (Gcc.LDF lbl'); Egcc.I (Gcc.AP 1)] @ last @
	 [Egcc.L lbl'] @ compExp lbl' vars' [Egcc.I Gcc.RTN] e2
  | Debug (e1, e2) ->
      compExp lbl vars [] e1 @ [Egcc.I (Gcc.DBUG)] @
      compExp lbl vars last e2

let compMeth (nm, args, e) =
  let vars = Hashtbl.create 10 in
    List.iteri (fun i arg -> Hashtbl.add vars arg (0, i)) args;
    [Egcc.L nm] @ compExp (nm) vars [Egcc.I Gcc.RTN] e

let compProg l = List.flatten (List.map compMeth l)
