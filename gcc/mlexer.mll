{
  open Mparser
}
rule token = parse
  [' ' '\t'] { token lexbuf }
| [';'] [^'\n']* ['\n']+ { token lexbuf }
| '+' { ADD }
| '-' { SUB }
| '*' { MUL }
| '/' { DIV }
| '=' { EQ }
| '>' { GT }
| '<' { LT }
| ">=" { CGTE }
| '(' { LPAREN }
| ')' { RPAREN }
| "atom" { ATOM }
| "fst" { FST }
| "snd" { SND }
| "if" { IF }
| "then" { THEN }
| "else" { ELSE }
| "fun" { FUN }
| "debug" { DEBUG }
| "let" { LET }
| "in" { IN }
| ',' { COMMA }
| "->" { TO }
| ['-']? ['0'-'9']+ as lxm { INT(int_of_string lxm) }
| ['a'-'z']['a'-'z' 'A'-'Z' '1'-'9' '_']* as lxm { NAME(lxm) }
| ['\n']+ { token lexbuf }
| eof { EOF }
