%token <int> INT
%token <string> NAME
%token ADD SUB MUL DIV EQ LT GT CGTE LPAREN RPAREN
%token ATOM FST SND LET IN DEBUG
%token IF THEN ELSE FUN TO COMMA COLON EOF
%left ADD SUB
%left MUL DIV
%right COMMA
%left EQ GT LT CGTE
%start prog
%type <Macros.prg> prog
%%
prog:
 | EOF       { [] }
 | meth prog { $1::$2 }
;
meth:
 | LET NAME LPAREN args RPAREN EQ expr { ($2, $4, $7) }
;
args:
 |                 { [] }
 | NAME            { [$1] }
 | NAME COMMA args { $1::$3 }
;
expr:
 | INT       { Macros.Lit($1) }
 | NAME      { Macros.Var($1) }
 | LPAREN expr RPAREN { $2 }
 | LPAREN expr COMMA expr RPAREN { Macros.Binop(Macros.Cons, $2, $4) }
 | FST expr  { Macros.Unop(Macros.Fst, $2) }
 | SND expr  { Macros.Unop(Macros.Snd, $2) }
 | ATOM expr  { Macros.Unop(Macros.Atom, $2) }
 | expr ADD expr { Macros.Binop(Macros.Add, $1, $3) }
 | expr SUB expr { Macros.Binop(Macros.Sub, $1, $3) }
 | expr MUL expr { Macros.Binop(Macros.Mul, $1, $3) }
 | expr DIV expr { Macros.Binop(Macros.Div, $1, $3) }
 | expr EQ expr { Macros.Binop(Macros.Eq, $1, $3) }
 | expr LT expr { Macros.Binop(Macros.Gt, $3, $1) }
 | expr GT expr { Macros.Binop(Macros.Gt, $1, $3) }
 | NAME LPAREN exprl RPAREN { Macros.Call($1, $3) }
 | IF expr THEN expr ELSE expr { Macros.If($2, $4, $6) }
 | LET NAME EQ expr IN expr { Macros.Let($2, $4, $6) }
 | DEBUG expr IN expr { Macros.Debug($2, $4) }
;
exprl:
 |                  { [] }
 | expr             { [$1] }
 | expr COMMA exprl { $1::$3 }
;
