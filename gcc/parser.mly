%token <int> INT
%token <string> LABEL
%token LDC LD ADD SUB MUL DIV CEQ CGT CGTE ATOM CONS CAR CDR SEL JOIN LDF AP RTN DUM RAP STOP TSEL TAP TRAP ST DBUG BRK
%token NL COLON EOF
%start prg
%type <Egcc.prg> prg
%%
prg:
   EOF               { [] }
|  NL prg            { $2 }
|  line NL prg       { $1::$3 }
;
line:
   LABEL COLON       { Egcc.L($1) }
 | inst              { Egcc.I($1) }
;
inst:
 | LDC INT           { Gcc.LDC($2) }
 | LD INT INT        { Gcc.LD($2,$3) }
 | ADD               { Gcc.ADD }
 | SUB               { Gcc.SUB }
 | MUL               { Gcc.MUL }
 | DIV               { Gcc.DIV }
 | CEQ               { Gcc.CEQ }
 | CGT               { Gcc.CGT }
 | CGTE              { Gcc.CGTE }
 | ATOM              { Gcc.ATOM }
 | CONS              { Gcc.CONS }
 | CAR               { Gcc.CAR }
 | CDR               { Gcc.CDR }
 | SEL LABEL LABEL   { Gcc.SEL($2, $3) }
 | JOIN              { Gcc.JOIN }
 | LDF LABEL         { Gcc.LDF($2) }
 | AP INT            { Gcc.AP($2) }
 | RTN               { Gcc.RTN }
 | DUM INT           { Gcc.DUM($2) }
 | RAP INT           { Gcc.RAP($2) }
 | STOP              { Gcc.STOP }
 | TSEL LABEL LABEL  { Gcc.TSEL($2, $3) }
 | TAP INT           { Gcc.TAP($2) }
 | TRAP INT          { Gcc.TRAP($2) }
 | ST INT INT        { Gcc.ST($2, $3) }
 | DBUG              { Gcc.DBUG }
 | BRK               { Gcc.BRK }
;
