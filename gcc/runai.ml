open Batteries
open Game
open Sim
open Gamesim

let map_str = 
"########
#%.#...#
#.#= #.#
#.####.#
#o.....#
##.##.##
#......#
#.####.#
#..\\...#
########"

let not_wall world pos =
  match Hashtbl.find world.map pos with
  | Wall -> false
  | _ -> true

let add_pos (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

let new_pos world dir =
  let diff_dir = 
    match dir with
    | 0 -> (0, -1)
    | 1 -> (1, 0)
    | 2 -> (0, 1)
    | 3 -> (-1, 0)
    | _ -> failwith "invalid direction"
  in add_pos world.lman.llocation diff_dir



let _ =
  let world = fresh_world (build_map map_str) in
  let lambda_ai_step = setup_lambda_ai (open_in Sys.argv.(1)) world in
  world.lambda_ai <- lambda_ai_step;
  while true do
    let new_dir = lambda_ai_step world in
    print_world world;
    let pos' = pos_after_move world.lman.llocation new_dir in
    if not_wall world pos' then (
      world.lman.llocation <- pos';
      world.lman.ldirection <- new_dir
    ) else
      failwith (Printf.sprintf "cannot move %s" (dir_to_str new_dir));
    Printf.printf "AI wants to move %s\n" (dir_to_str new_dir);
  done
