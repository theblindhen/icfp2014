open Batteries

open Gcc

type code_ptr = int
type env_ptr = int

type value =
  | Int of int
  | Pair of value * value
  | Closure of code_ptr * env_frame
 and env_frame = {
   parent         : env_frame option;
   mutable data   : (value Array.t) option;
   size           : int;
 } 

type ctr =
  | Ret of code_ptr * env_frame
  | Join of code_ptr
  | Stop

type memory = {
  mutable pc        : code_ptr;
  data              : value Stack.t;
  control           : ctr Stack.t;
  mutable env       : env_frame;
}

exception StopExn

let kill_debug_output = true

let rec pr_val = function
  | Int i -> Printf.printf "Int(%d)" i
  | Pair (x, y) -> Printf.printf "Pair("; pr_val x; Printf.printf ", "; pr_val y; Printf.printf ")"
  | Closure (f, fp) -> Printf.printf "Closure(%d, env)" f

let runInst m = function
  | LDC n -> 
     Stack.push (Int n) m.data;
     m.pc <- m.pc + 1
  | LD (n, i) ->
     let fp = ref m.env in
     let n = ref n in
     while !n > 0 do 
       (match (!fp).parent with
	| Some e -> fp := e
	| None -> failwith "insufficient frames");
       n := !n - 1
     done;
     (match (!fp).data with
      | Some vals -> 
	 (try
	     Stack.push (vals.(i)) m.data;
	     m.pc <- m.pc + 1
	   with 
	   | Not_found -> failwith ("insufficient frame elements"))
      | None -> failwith "FRAME MISMATCH")
  | ADD 
  | SUB 
  | MUL 
  | DIV 
  | CEQ
  | CGT
  | CGTE as op ->
     let y = Stack.pop m.data in
     let x = Stack.pop m.data in
     (match (x, y) with
      | (Int x, Int y) ->
	 let v = match op with
	   | ADD -> x + y
	   | SUB -> x - y
	   | MUL -> x * y
	   | DIV -> x / y
	   | CEQ -> if x == y then 1 else 0
	   | CGT -> if x > y then 1 else 0
	   | CGTE -> if x >= y then 1 else 0
	   | _ -> failwith "cannot happen" in
	 Stack.push (Int v) m.data;
	 m.pc <- m.pc + 1
      | _ -> failwith "TAG MISMATCH")
  | ATOM ->
     let v =
       match Stack.pop m.data with
       | Int _ -> 1 | _ -> 0 in 
     Stack.push (Int v) m.data;
     m.pc <- m.pc + 1
  | CONS ->
     let y = Stack.pop m.data in
     let x = Stack.pop m.data in
     Stack.push (Pair (x, y)) m.data;
     m.pc <- m.pc + 1
  | CAR
  | CDR as op ->
     let x = Stack.pop m.data in
     (match x with
      | Pair (x, y) ->
	 let v = match op with
	   | CAR -> x
	   | CDR -> y
	   | _ -> failwith "cannot happen" in
	 Stack.push v m.data;
	 m.pc <- m.pc + 1
      | _ -> failwith "TAG MISMATCH")
  | SEL (t, f) ->
     let x = Stack.pop m.data in
     (match x with
      | Int x ->
	 Stack.push (Join (m.pc + 1)) m.control;
	 if x = 0 then
	   m.pc <- f
	 else
	   m.pc <- t
      | _ -> failwith "TAG MISMATCH")
  | JOIN ->
     (match Stack.pop m.control with
      | Join i -> 
	 m.pc <- i
      | _ -> failwith "CONTROL MISMATCH")
  | LDF f ->
     Stack.push (Closure (f, m.env)) m.data;
     m.pc <- m.pc + 1
  | AP n ->
     let x = Stack.pop m.data in
     (match x with
      | Closure (f, e) ->
	 let vals = Array.create n (Int 0) in
	 let i = ref (n - 1) in
	 while !i <> -1 do
	   let y = Stack.pop m.data in
	   vals.(!i) <- y;
	   i := !i - 1
	 done;
	 let fp = { parent = Some e; data = Some vals; size = n } in
	 Stack.push (Ret (m.pc + 1, m.env)) m.control; (* DRAGONS *)
	 m.env <- fp;
	 m.pc <- f
      | _ -> failwith "TAG MISMATCH")
  | RTN ->
     (match (Stack.pop m.control) with
      | Stop ->
          raise StopExn
      | Ret (ret_addr, ret_env) ->
	 m.env <- ret_env;
	 m.pc <- ret_addr
      | _ -> failwith "TAG MISMATCH")
  | DUM n ->
     let fp = { parent = Some m.env; data = None; size = n } in
     m.env <- fp;
     m.pc <- m.pc + 1
  | RAP n -> 
     (match Stack.pop m.data with
      | Closure (f, fp) ->
	 (match m.env.data with
	  | None ->
	     if m.env.size == n then 
               if m.env == fp then
                 let i = ref (n- 1) in
		 let vals = Array.create n (Int 0) in
		 while !i <> -1 do
		   let y = Stack.pop m.data in
		   vals.(!i) <- y;
		   i := !i - 1
		 done;
		 fp.data <- Some vals;
		 (match m.env.parent with
		  | Some ep ->
		     Stack.push (Ret (m.pc + 1, ep)) m.control;
		     m.env <- fp;
		     m.pc <- f
		  | None ->
		     failwith "insufficient frame stacks")
               else failwith "FRAME MISMATCH"
             else failwith "FRAME MISMATCH"
	  | Some _ ->
	     failwith "FRAME MISMATCH")
      | _ -> failwith "TAG MISMATCH")
  | STOP -> failwith "NOT SUPPORTED"
  | BRK -> 
     m.pc <- m.pc + 1
  | DBUG ->
     let x = Stack.pop m.data in
     if kill_debug_output then () else (pr_val x);
     m.pc <- m.pc + 1
  | TSEL (t, f) ->
     (match (Stack.pop m.data) with
      | Int x ->
	 if x = 0 then 
	   m.pc <- f
	 else
	   m.pc <- t
      | _ -> failwith "TAG MISMATCH")
  | ST (n, j) ->
     let fp = ref m.env in
     let i = ref n in
     while !i > 0 do
       (match (!fp).parent with
	| Some e -> fp := e
	| None -> failwith "insufficient frames");
       i := !i + 1
     done;
     (match (!fp).data with
      | Some vals ->
	 let v = Stack.pop m.data in
	 vals.(j) <- v;
	 m.pc <- m.pc + 1
      | None -> failwith "FRAME MISMATCH")
  | _ -> failwith "NOT SUPPORTED"

let step m prg = runInst m (prg.(m.pc))

let pr_ctr = function
  | Join f -> Printf.printf "Join(%d)" f
  | Ret (f, fp) -> Printf.printf "Ret(%d, env)" f
  | Stop -> Printf.printf "Stop"

let pr_state m prg =
  Printf.printf "PC: %d (%s)\n" m.pc (Gcc.inst_to_str string_of_int (prg.(m.pc)));
  Stack.print ~first:"Data stack: " ~last:"\n" ~sep:"," (fun out elm -> pr_val elm) IO.stdout m.data;
  Stack.print ~first:"Control stack: " ~last:"\n" ~sep:"," (fun out elm -> pr_ctr elm) IO.stdout m.control

let run mem debug prg =
  let insts_exec = ref 0 in
  let m = match mem with
    | Some m -> m
    | None -> { pc = 0; data = Stack.create (); control = Stack.create (); env = { parent = None; data = None; size = 0 } } in
  Stack.push Stop m.control;
  try
    while true do
      if debug then pr_state m prg else ();
      step m prg;
      insts_exec := !insts_exec + 1
    done;
    (Stack.pop m.data, m)
  with StopExn ->
    if debug then 
      (pr_state m prg;
       Printf.printf "Total number of instructions executed: %d\n" (!insts_exec))
    else ();
    (Stack.pop m.data, m)

(*
 | TAP of literal
 | TRAP of literal
*)
