open Batteries

let _ =
  let lexbuf = Lexing.from_channel (open_in Sys.argv.(1)) in
  let result = Parser.prg Lexer.token lexbuf in
    Printf.printf "Parsed program:\n%s\n" (dump result);
    Sim.run None true (Array.of_list (Egcc.compile result))
