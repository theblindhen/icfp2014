open Batteries

let gen_list ns = 
  let lcds = List.map (fun n -> Egcc.I ( Gcc.LDC n )) ns
  let cons = List.init (fun _ -> Egcc.I ( Gcc.CONS )) (List.length ns)
  lcds @ cons


