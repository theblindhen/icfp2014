open Batteries

type reg = A | B | C | D | E | F | G | H

type arg =
  | Reg of reg
  | Indir of reg
  | Const of int
  | Loc of int

type bop =
  | MOV | ADD | SUB | MUL | DIV | AND | OR | XOR

type uop =
  | INC | DEC | INT

type jop =
  | JLT | JEQ | JGT

type 'a inst =
  | Bin of bop * arg * arg
  | Un of uop * arg
  | Hlt
  | Jmp of jop * 'a * arg * arg

let map_inst f = function
  | Bin (b, r1, r2) -> Bin (b, r1, r2)
  | Un (u, r) -> Un (u, r)
  | Hlt -> Hlt
  | Jmp (j, a, r1, r2) -> Jmp (j, f a, r1, r2)

type prg = int inst list

type label = string

type line = 
  | L of label
  | I of label inst

type eprg = line list

let compile (prg : eprg) :prg =
  let count = ref 0 in
  let lmap = Hashtbl.create 10 in
    List.iter
      (function
	| L l ->
            if Hashtbl.mem lmap l then
              failwith "label declared more than once"
            else
              Hashtbl.add lmap l !count
        | I i -> count := !count + 1) prg;
    List.filter_map 
      (function 
	| L _ -> None
	| I i -> Some (map_inst (fun l -> 
		   if Hashtbl.mem lmap l then 
		     Hashtbl.find lmap l 
		   else 
		     failwith (Printf.sprintf "label %s not declared" l)) i)) prg

let reg_to_str = function
  | A -> "A"
  | B -> "B"
  | C -> "C"
  | D -> "D"
  | E -> "E"
  | F -> "F"
  | G -> "G"
  | H -> "H"

let arg_to_str = function
  | Reg r -> reg_to_str r
  | Indir r -> "[" ^ (reg_to_str r) ^ "]"
  | Const c -> string_of_int c
  | Loc c -> "[" ^ (string_of_int c) ^ "]"

let bop_to_str = function
  | MOV -> "MOV"
  | ADD -> "ADD"
  | SUB -> "SUB"
  | MUL -> "MUL" 
  | DIV -> "DIV"
  | AND -> "AND" 
  | OR  -> "OR"
  | XOR -> "XOR"

let uop_to_str = function
  | INC -> "INC"
  | DEC -> "DEC"
  | INT -> "INT"

let jop_to_str = function
  | JLT -> "JLT"
  | JEQ -> "JEQ"
  | JGT -> "JGT"

let inst_to_str a_to_str = function
  | Bin(b, r1, r2) ->
     (bop_to_str b) ^ " " ^ (arg_to_str r1) ^ ", " ^ (arg_to_str r2)
  | Un(u, r) ->
     (uop_to_str u) ^ " " ^ (arg_to_str r)
  | Hlt -> "HLT"
  | Jmp(j, a, r1, r2) ->
     (jop_to_str j) ^ " " ^ (a_to_str a) ^ ", " ^ (arg_to_str r1) ^ ", " ^ (arg_to_str r2)

let prg_to_str a_to_str prg =
  List.fold_left
    (fun acc inst ->
       (if acc <> "" then acc ^ "\n" else "") ^ (inst_to_str a_to_str inst))
    "" prg
