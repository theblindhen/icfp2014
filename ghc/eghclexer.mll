{
  open Eghcparser
  open Lexing

  let pos lexbuf = (lexeme_start lexbuf, lexeme_end lexbuf)

  let advance_line_pos pos =
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum; }

  let advance_line lexbuf =
    lexbuf.lex_curr_p <- advance_line_pos lexbuf.lex_curr_p
                       
}
rule token = parse
  [' ' '\t'] { token lexbuf }
| [';'] [^'\n']* ['\n'] { advance_line lexbuf ; NL }
| "//" [^'\n']* ['\n'] { advance_line lexbuf ; NL }
| ['#'] [^'\n']* ['\n'] { advance_line lexbuf ; NL }  (*Directive*)
| '|' { NL }
| ['-']? ['0'-'9']+ as lxm { INT(int_of_string lxm) }
| 'A'   { A }
| 'a'   { A }
| 'B'   { B }
| 'b'   { B }
| 'C'   { C }
| 'c'   { C }
| 'D'   { D }
| 'd'   { D }
| 'E'   { E }
| 'e'   { E }
| 'F'   { F }
| 'f'   { F }
| 'G'   { G }
| 'g'   { G }
| 'H'   { H }
| 'h'   { H }
| "MOV" { MOV }
| "mov" { MOV }
| "ADD" { ADD }
| "add" { ADD }
| "SUB" { SUB }
| "sub" { SUB }
| "MUL" { MUL }
| "mul" { MUL }
| "DIV" { DIV }
| "div" { DIV }
| "AND" { AND }
| "and" { AND }
| "OR" { OR }
| "or" { OR }
| "XOR" { XOR }
| "xor" { XOR }
| "INC" { INC }
| "inc" { INC }
| "DEC" { DEC }
| "dec" { DEC }
| "INT" { INTR }
| "int" { INTR }
| "HLT" { HLT }
| "hlt" { HLT }
| "JLT" { JLT }
| "jlt" { JLT }
| "JGT" { JGT }
| "jgt" { JGT }
| "JEQ" { JEQ }
| "jeq" { JEQ }
| ','   { COMMA }
| ':'   { COLON }
| '['   { LBRACK }
| ']'   { RBRACK }
| ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '_' '0'-'9']* as lxm { LABEL(lxm) }
| ['\n'] { advance_line lexbuf ; NL }
| eof { EOF }
