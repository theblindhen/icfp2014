%token <int> INT
%token <string> LABEL
%token A B C D E F G H MOV ADD SUB MUL DIV AND OR XOR INC DEC INTR HLT JLT JEQ JGT LBRACK RBRACK
%token NL COLON EOF COMMA
%start prg
%type <Eghc.eprg> prg
%%
prg:
   EOF               { [] }
|  NL prg            { $2 }
|  line NL prg       { $1::$3 }
;
line:
   LABEL COLON       { Eghc.L($1) }
 | inst              { Eghc.I($1) }
;
inst:
 | binop arg COMMA arg            { Eghc.Bin($1, $2, $4) }
 | unop arg                       { Eghc.Un($1, $2) }
 | HLT                            { Eghc.Hlt }
 | jop LABEL COMMA arg COMMA arg  { Eghc.Jmp($1, $2, $4, $6) }
;
binop:
 | MOV { Eghc.MOV }
 | ADD { Eghc.ADD }
 | SUB { Eghc.SUB }
 | MUL { Eghc.MUL }
 | DIV { Eghc.DIV }
 | AND { Eghc.AND }
 | OR  { Eghc.OR  }
 | XOR { Eghc.XOR }
;
unop:
 | INC { Eghc.INC }
 | DEC { Eghc.DEC }
 | INTR { Eghc.INT }
;
jop:
 | JLT { Eghc.JLT }
 | JEQ { Eghc.JEQ }
 | JGT { Eghc.JGT }
;
arg:
 | reg                { Eghc.Reg($1) }
 | LBRACK reg RBRACK  { Eghc.Indir($2) }
 | INT                { Eghc.Const($1) }
 | LBRACK INT RBRACK  { Eghc.Loc($2) }
reg:
 | A { Eghc.A }
 | B { Eghc.B }
 | C { Eghc.C }
 | D { Eghc.D }
 | E { Eghc.E }
 | F { Eghc.F }
 | G { Eghc.G }
 | H { Eghc.H }

