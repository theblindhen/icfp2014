open Batteries

exception AsmParserError of int * int * string * string
let () =
  let input =
    match Array.length Sys.argv with
    | 1 -> IO.stdin
    | 2 -> open_in Sys.argv.(1)
    | n -> failwith "Wrong number of args"
  in
  let lexbuf = Lexing.from_channel input in
  try
    let result = Eghcparser.prg Eghclexer.token lexbuf in
      print_endline (Eghc.prg_to_str string_of_int (Eghc.compile result))
  with exn ->
    let curr = lexbuf.Lexing.lex_curr_p in
    let line = curr.Lexing.pos_lnum in
    let cnum = curr.Lexing.pos_cnum - curr.Lexing.pos_bol in
    let tok = Lexing.lexeme lexbuf in
    (*let tail = Parser.ruleTail "" lexbuf in*)
    raise (AsmParserError (line,cnum,tok,Printexc.to_string exn))
