open Eghc
open Game

type value = int

type memory = {
  mutable pc         : int;
  regs               : (reg, value) Hashtbl.t;
  heap               : value array;
  mutable new_dir    : int option;
  mutable insts_exec : int;
}

let init_mem () =
  let regs = Hashtbl.create 10 in
  List.iter (fun reg -> Hashtbl.add regs reg 0) [A; B; C; D; E; F; G; H];
  let heap = Array.create 256 0 in
    { pc = 0; regs = regs; heap = heap; new_dir = None; insts_exec = 0 }

exception HltExn

let load mem = function
  | Reg r -> Hashtbl.find mem.regs r
  | Indir r -> 
     let loc = Hashtbl.find mem.regs r in
       mem.heap.(loc)
  | Const c -> c
  | Loc c -> mem.heap.(c)

let store mem v = function
  | Reg r -> Hashtbl.replace mem.regs r v
  | Indir r ->
     let loc = Hashtbl.find mem.regs r in
       mem.heap.(loc) <- v
  | Const c -> failwith "invalid destination"
  | Loc c -> mem.heap.(c) <- v

let overflow v = 
  if v < 0 then 256 + (v mod 256) else v mod 256 (* DRAGONS: I'm too tired to figured out whether this is correct *)

let pr_mem mem (prg : int Eghc.inst array) =
  Printf.printf
    "%d: registers: A = %d, B = %d, C = %d, D = %d, E = %d, F = %d, G = %d, H = %d, PC = %d (%s), req_dir = %s\n" 
    (mem.insts_exec) (load mem (Reg A)) (load mem (Reg B)) (load mem (Reg C)) (load mem (Reg D))
    (load mem (Reg E)) (load mem (Reg F)) (load mem (Reg G)) (load mem (Reg H)) mem.pc
    (inst_to_str string_of_int (prg.(mem.pc))) 
    (match mem.new_dir with
     | None -> "?"
     | Some d -> string_of_int d)

let rec run_inst ghost_index mem world (prg : int Eghc.inst array) = function
  | Bin(MOV, dst, src) -> 
     store mem (load mem src) dst;
     mem.pc <- mem.pc + 1
  | Bin(op, dst, src)  -> 
     let v1 = load mem dst in
     let v2 = load mem src in 
     let v' = match op with
       | ADD -> v1 + v2 
       | SUB -> v1 - v2 
       | MUL -> v1 * v2 
       | DIV -> if v2 = 0 then failwith "division by zero" else v1 / v2 
       | AND -> v1 land v2 
       | OR  -> v1 lor  v2
       | XOR -> v1 lxor v2
       | _ -> failwith "not possible" in
     store mem (overflow v') dst;
     mem.pc <- mem.pc + 1
  | Un(INC, dst) -> 
     store mem (overflow (load mem dst + 1)) dst;
     mem.pc <- mem.pc + 1
  | Un(DEC, dst) -> 
     store mem (overflow (load mem dst - 1)) dst;
     mem.pc <- mem.pc + 1
  | Un(INT, Const i) -> 
     (match i with
     | 0 ->
	mem.new_dir <- Some (load mem (Reg A))
     | 1 ->
	store mem (fst world.lman.llocation) (Reg A);
	store mem (snd world.lman.llocation) (Reg B);
     | 2 ->
	store mem (fst world.lman.llocation) (Reg A);
	store mem (snd world.lman.llocation) (Reg B);
     | 3 ->
	store mem ghost_index (Reg A);
     | 4 ->
	let gh_index = load mem (Reg A) in
	(try
	    let ghost = List.nth world.ghosts gh_index in
	    store mem (fst ghost.gstart_location) (Reg A);
	    store mem (snd ghost.gstart_location) (Reg B);
	  with _ -> ())
     | 5 ->
	let gh_index = load mem (Reg A) in
	(try
	    let ghost = List.nth world.ghosts gh_index in
	    store mem (fst ghost.glocation) (Reg A);
	    store mem (snd ghost.glocation) (Reg B);
	  with _ -> ())
     | 6 ->
	let gh_index = load mem (Reg A) in
	(try
	    let ghost = List.nth world.ghosts gh_index in
	    store mem (int_of_gh_vitality ghost.gvitality) (Reg A);
	    store mem (dir_to_int ghost.gdirection) (Reg B);
	  with _ -> ())
     | 7 ->
	let x = load mem (Reg A) in
	let y = load mem (Reg B) in
	let map_cell = Hashtbl.find world.map (x, y) in
	store mem (encode_cell map_cell) (Reg A)
     | 8 ->
	pr_mem mem prg
     | _ -> failwith "invalid interrupt");
     mem.pc <- mem.pc + 1
  | Un(INT, _) -> failwith "invalid interrupt"
  | Jmp(jop, target, x, y) ->
     let vx = load mem x in
     let vy = load mem y in
     let jump = 
       match jop with
       | JLT -> vx < vy
       | JEQ -> vx = vy
       | JGT -> vx > vy
     in
     if jump then 
       mem.pc <- target 
     else 
       mem.pc <- mem.pc + 1
  | Hlt ->
     raise HltExn

let step ghost_index mem world prg =
  run_inst ghost_index mem world prg (prg.(mem.pc))

let run ghost_index debug mem world prg =
  let m = 
    match mem with
    | Some m -> m 
    | None -> init_mem ()
  in 
  try
    m.pc <- 0;
    m.insts_exec <- 0;
    m.new_dir <- None;
    while true do
      if debug then pr_mem m prg else ();
      step ghost_index m world prg;
      m.insts_exec <- m.insts_exec + 1;
      if m.insts_exec = 1024 then
	raise HltExn
      else 
	()
    done; m
  with HltExn -> m
