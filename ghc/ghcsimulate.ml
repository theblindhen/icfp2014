open Batteries
open Eghc
open Game
open Gamesim

let map_str = 
"########
#%.#...#
#.#= #.#
#.####.#
#o.....#
##.##.##
#......#
#.####.#
#..\\...#
########"

(* TODO: cleanup once more stuff has been moved into game.ml *)

let _ =
  let input =
    match Array.length Sys.argv with
    | 1 -> IO.stdin
    | 2 -> open_in Sys.argv.(1)
    | n -> failwith "Wrong number of args"
  in
  let world = fresh_world (build_map map_str) in
  let ghost_ai_step = setup_ghost_ai input 0 in
    while true do
      match ghost_ai_step world with
      | Some new_dir ->
	 Printf.printf "ghost AI wants to move %s\n" (dir_to_str new_dir);
      | None -> 
	 Printf.printf "ghost AI didn't try to make a move\n"
    done
