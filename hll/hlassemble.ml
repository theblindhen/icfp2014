open Batteries

exception HllParserError of int * int * string * string
let _ =
  let input =
    match Array.length Sys.argv with
    | 1 -> IO.stdin
    | 2 -> open_in Sys.argv.(1)
    | n -> failwith "Wrong number of args" in
  let lexbuf = Lexing.from_channel input in
  let result =
    try
      let result = Hlparser.prg Hllexer.token lexbuf in
      (* Printf.eprintf "Parsed program:\n%s\n" (Sweet.string_of_sweet result); *)
      (* Printf.eprintf "Compiled program:\n%!"; *)
      result
    with exn ->
      let curr = lexbuf.Lexing.lex_curr_p in
      let line = curr.Lexing.pos_lnum in
      let cnum = curr.Lexing.pos_cnum - curr.Lexing.pos_bol in
      let err_file = curr.Lexing.pos_fname in
      let tok = Lexing.lexeme lexbuf in
      (*let tail = Parser.ruleTail "" lexbuf in*)
      let orig_file = match Array.length Sys.argv with
                      | 1 -> "stdin" | 2 -> Sys.argv.(1) | _ -> "ERR" in
      output_string stderr ("Parser error while parsing "^ orig_file ^"\n");
      output_string stderr (Printf.sprintf "Failure in file %s, line %d, col %d. Last token %s\n" err_file line cnum tok);
      raise (HllParserError (line,cnum,tok,Printexc.to_string exn))
  in
  Sweet.desugar result
  |> Sour.compile
  |> Egcc.prg_to_str
  |> print_endline
