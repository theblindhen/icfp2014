{
  open Hlparser
  open Lexing

  exception UnexpectedToken of char
         
  let pos lexbuf = (lexeme_start lexbuf, lexeme_end lexbuf)

  let advance_line_pos pos =
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum; }

  let advance_line lexbuf =
    lexbuf.lex_curr_p <- advance_line_pos lexbuf.lex_curr_p

  let parse_directive lexbuf dir =
    try
      Scanf.sscanf dir "# %d %s %d" (fun line file _ ->
        lexbuf.lex_curr_p <- { pos_fname = file; pos_lnum = line; pos_bol = 0; pos_cnum = 0 })
    with exn ->
      ()


}
rule token = parse
  [' ' '\t'] { token lexbuf }
| [';'] [^'\n']* ['\n'] { advance_line lexbuf ; token lexbuf }
| "//" [^'\n']* ['\n'] { advance_line lexbuf ; token lexbuf }
| ['\n'] { advance_line lexbuf ; token lexbuf }
| ['#'] [^'\n']* ['\n'] as dir { parse_directive lexbuf dir ; token lexbuf }  (*CPP directive*)
| '+' { ADD }
| '-' { SUB }
| '*' { MUL }
| '/' { DIV }
| '=' { CEQ }
| '>' { CGT }
| '<' { CLT }
| ">=" { CGTE }
| "<=" { CLTE }
| '(' { LPAREN }
| ')' { RPAREN }
| '[' { LSQBRACK }
| ']' { RSQBRACK}
| '{' { LCBRACK }
| '}' { RCBRACK }
| ',' { COMMA }
| "atom" { ATOM }
| "car" { CAR }
| "fst" { CAR }
| "cdr" { CDR }
| "snd" { CDR }
| "if" { IF }
| "then" { THEN }
| "else" { ELSE }
| "fun" { FUN }
| "fix" { FIX }
| "debug" { DEBUG }
| "break" { BREAK }
| "let" { LET }
| "rec" { REC }
| "and" { AND }
| "in" { IN }
| "->" { TO }
| ['~']? ['0'-'9']+ as lxm
    { if String.get lxm 0 = '~'
      then INT(-int_of_string (String.sub lxm 1 (String.length lxm - 1)))
      else INT(int_of_string lxm) }
| ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '_' '0'-'9' '\'']* as lxm { VAR(lxm) }
| eof { EOF }
| _ as lxm { raise (UnexpectedToken lxm)}
