%{
  let make_decl xs e =
    match xs with
    | [x] -> `Const (x, e)
    | x :: xs -> `Fun (x, xs, e)
    | [] -> failwith "Empty let block??"
%}
%token <int> INT
%token <string> VAR
%token ADD SUB MUL DIV CEQ CGT CLT CGTE CLTE LPAREN RPAREN LSQBRACK RSQBRACK LCBRACK RCBRACK
%token ATOM CAR CDR
%token IF THEN ELSE FUN TO COMMA COLON EOF FIX DEBUG BREAK IN AND LET REC
%left ADD SUB
%left MUL DIV
%right COMMA
%left CEQ CGT CGTE
%right ATOM CAR CDR
%start prg
%type <Sweet.sweet> prg
%%
prg:
   expr EOF                            { $1 }
;
expr:
 | INT                                 { `Int ( $1 ) }
 | VAR                                 { `Var( $1 ) }
 | LPAREN exprlist RPAREN              { let rec conscat = function
                                         | e1 :: e2 :: el -> 
                                           let e_rec = conscat (e2 :: el) in
                                             `Binop( Sour.Cons, e1, e_rec )
                                         | [e]            -> e
                                         | _              -> failwith "empty exprlist" in
                                           conscat $2 }
 | LSQBRACK RSQBRACK                   { `Int( 0 ) }
 | LSQBRACK exprlist RSQBRACK          { let rec conscat = function
                                         | e1 :: e2 :: el -> 
                                           let e_rec = conscat (e2 :: el) in
                                             `Binop( Sour.Cons, e1, e_rec )
                                         | [e]            -> e
                                         | _              -> failwith "empty exprlist" in
                                           conscat ($2 @ [ `Int( 0 ) ]) }
 | expr ADD   expr                     { `Binop( Sour.Add , $1, $3 ) }
 | expr SUB   expr                     { `Binop( Sour.Sub , $1, $3 ) }
 | expr MUL   expr                     { `Binop( Sour.Mul , $1, $3 ) }
 | expr DIV   expr                     { `Binop( Sour.Div , $1, $3 ) }
 | expr CEQ   expr                     { `Binop( Sour.Ceq , $1, $3 ) }
 | expr CGT   expr                     { `Binop( Sour.Cgt , $1, $3 ) }
 | expr CGTE  expr                     { `Binop( Sour.Cgte, $1, $3 ) }
 | expr CLT   expr                     { `Binop( Sour.Cgt , $3, $1 ) }
 | expr CLTE  expr                     { `Binop( Sour.Cgte, $3, $1 ) }
 | ATOM expr                           { `Unop( Sour.Atom, $2 ) }
 | CAR expr                            { `Unop( Sour.Car, $2 ) }
 | CDR expr                            { `Unop( Sour.Cdr, $2 ) }
 | IF expr THEN expr ELSE expr         { `If( $2, $4, $6 )}
 | FUN varlist TO expr                 { `Lam( $2, $4 ) }
 | expr LPAREN exprlist RPAREN         { `App( $1, $3 ) }
 | FIX VAR varlist TO expr             { `LetRec ([`Fun ($2, $3, $5)],
                                                   `Var $2) }
 | DEBUG expr IN expr                  { `Dbug( $2, $4 ) }
 | BREAK IN expr                       { `Brk( $3 ) }
 | LET deflist IN expr                 { `Let($2, $4) }
 | LET REC deflist IN expr             { `LetRec($3, $5) }
; 
exprlist:
 | expr                                { [ $1 ] }
 | expr COMMA exprlist                 { $1 :: $3 }
;
varlist:
 | VAR                                 { [ $1 ] }
 | VAR varlist                         { $1 :: $2 }
;
deflist:
 | varlist CEQ expr                    { [ make_decl $1 $3 ] }
 | varlist CEQ expr AND deflist        { make_decl $1 $3 :: $5 }
