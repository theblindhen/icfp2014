open Batteries

open Egcc
open Gcc

type binop = Add | Sub | Mul | Div | Ceq | Cgt | Cgte | Cons

type unop = Atom | Car | Cdr
                          
type 'a sour' = [
  | `Int of int
  | `Var of string
  | `Binop of binop * 'a * 'a
  | `Unop of unop * 'a
  | `If of 'a * 'a * 'a
  | `App of 'a * 'a list
  | `Lam of string list * 'a
  | `LetRec of 'a decl' list * 'a
  | `Dbug of 'a * 'a (* print first arg, continue with second *)
  | `Brk of 'a
]
and 'a decl' = [
  | `Fun of string * string list * 'a
  | `Const of string * 'a
]

type sour = sour sour'

exception SemanticError of string * string list

let spr = Printf.sprintf

let bin_to_str = function
  | Add -> "+"
  | Sub -> "-"
  | Mul -> "*"
  | Div -> "/"
  | Ceq -> "="
  | Cgt -> ">"
  | Cgte -> ">="
  | Cons -> ","

let un_to_str = function
  | Atom -> "atom"
  | Car -> "car"
  | Cdr -> "cdr"


let compile_binop = function
  | Add -> ADD
  | Sub -> SUB
  | Mul -> MUL
  | Div -> DIV
  | Ceq -> CEQ
  | Cgt -> CGT
  | Cgte -> CGTE
  | Cons -> CONS

let compile_unop = function
  | Atom -> ATOM
  | Car -> CAR
  | Cdr -> CDR

let decl_head_to_str = function
  | `Fun (f,_,_) -> "fun "^ f
  | `Const (x,_) -> "val "^ x

let prg_head_to_str = function
  | `Int i -> "int "^ string_of_int i
  | `Var x -> "var "^ x
  | `Binop (op,_,_) -> "binop "^ bin_to_str op
  | `Unop (op,_) -> "unop" ^ un_to_str op
  | `If (_,_,_) -> "if"
  | `App (_,_) -> "apply"
  | `Lam (opers,_) -> "fun "^ String.concat " " opers
  | `LetRec (decls,_) -> "let rec ("^ String.concat ", " (List.map decl_head_to_str decls) ^")"
  | `Dbug (_,_) -> "debug"
  | `Brk _ -> "break"

(* check given list of names for duplicates *)
let check_duplicates names =
  let name_map = Hashtbl.create 10 in
    List.iter (fun name -> if Hashtbl.mem name_map name then raise (SemanticError ("name "^ name ^" already declared", [])) else Hashtbl.add name_map name 0) names

let compile (t: sour) : Egcc.prg =
  let fresh =
    let counter = ref 0 in
    fun () -> incr counter; string_of_int !counter
  in
  let toplevel = Hashtbl.create 10 in
  let rec aux (env: string list list) prg =
    try
      let aux_d where env t = 
        try aux env t
        with SemanticError (m, stack) ->
          raise (SemanticError (m, ("in the "^ where)::stack)) in
      let self where = aux_d where env
      in
      match prg with
      | `Int n -> [ LDC n ]
      | `Var x ->
          let rec find_env n env =
            match env with
            | [] -> raise (SemanticError ("Could not find var: "^ x, []))
            | frame::env ->
                match List.index_of x frame with
                | Some i -> (n, i)
                | None -> find_env (n+1) env
          in
          let n, i = find_env 0 env in
          [ LD (n, i) ]
      | `Binop (binop, a, b) ->
          self "left" a @ self "right" b @ [ compile_binop binop ]
      | `Unop (unop, a) ->
          self "body" a @ [ compile_unop unop ]
      | `If (cond, t, f) ->
          let label_no = fresh () in
          let label_t = "if_t_" ^ label_no in
          let label_f = "if_f_" ^ label_no in
          Hashtbl.add toplevel label_t (self "true" t @ [JOIN]);
          Hashtbl.add toplevel label_f (self "false" f @ [JOIN]);
          self "cond" cond @ [SEL (label_t, label_f)]
      | `App (func, args) ->
          List.concat (List.mapi (fun i -> self ("arg "^ string_of_int i)) args) @
          self "body" func @
          [ AP (List.length args) ]
      | `Lam (params, body) ->
          let label = "lam_" ^ fresh () in
          check_duplicates params;
          Hashtbl.add toplevel label (
            aux_d "body" (params :: env) body @ [RTN]
          );
          [ LDF label ]
      | `LetRec (decls, body) -> (* funs are (name, params, body) *)
          let ndecls = List.length decls in
          let funno_map = Hashtbl.create ndecls in
          List.iter (function
                      | `Fun (f,_,_) -> Hashtbl.add funno_map f (f ^ "_" ^ fresh ())
                      | _ -> ()
                    ) decls;
          let label_entry = "rec_entry_"^ string_of_int ndecls ^"_"^ fresh () in
          let names = List.map (function `Fun (f,_,_) -> f | `Const (x,_) -> x) decls in
          check_duplicates names;
          let rap_env = names :: env in
          List.iter (function
                     | `Const _ -> ()
                     | `Fun (f, params, body) ->
          	      check_duplicates (f::params);
                        Hashtbl.add toplevel ("body_"^ Hashtbl.find funno_map f) (
                          aux_d ("body of fun "^ f) (params :: rap_env) body @ [RTN]
                        )
          ) decls;
          Hashtbl.add toplevel label_entry (
            (List.filteri_map (fun i -> function
              | `Const (x, body) -> Some (aux_d ("expr for "^ x) rap_env body @ [ ST (0,i) ])
              | _ -> None) decls |> List.concat)
            @ aux_d "body" rap_env body @ [ RTN ]
          );
          DUM ndecls :: List.map (function `Fun (f,_,_) -> LDF ("body_"^ Hashtbl.find funno_map f)
                                         | `Const (x,_) -> LDC 0) decls
                      @ [ LDF label_entry; RAP ndecls ]
      | `Dbug (output, continuation) ->
          self "output" output @ DBUG :: self "continuation" continuation
      | `Brk continuation ->
          [ BRK ] @ self "continuation" continuation
    with SemanticError (m, stack) ->
      raise (SemanticError (m, prg_head_to_str prg::stack))
      
  in
  let main =
    try
      aux [["initial_world"]] t @ [RTN] |> of_instrs
    with SemanticError (m,stack) ->
      output_string stderr ("Semantic error: "^ m ^"\n");
      output_string stderr ("Stack:\n\t"^ String.join " ;\n\t" stack ^"\n");
      failwith ("Semantic error: "^ m)
  in
  let procs =
    Hashtbl.enum toplevel
    |> List.of_enum
    |> List.sort (fun (l1,_) (l2,_) -> String.compare l1 l2)
    |> List.map (fun (label, instrs) -> L label :: of_instrs instrs)
    |> List.concat
  in
  main @ procs

let unittest () =
  let open OUnit2 in
  "Highlevel" >::: [
    "compile simple" >:: begin fun _ ->
      (* should calculate 4 *)
      let input : sour = `Binop (Sub, `Int 5, `Unop (Atom, `Int 3)) in
      let expected = of_instrs [ LDC 5; LDC 3; ATOM; SUB; RTN ] in
      let compiled = compile input in
      assert_equal ~printer:dump expected compiled
    end;
    "compile local_gcc" >:: begin fun _ ->
      (* should give essentially local.gcc *)
      let input : sour = `App (`Lam (["x"], `Binop (Add, `Var "x", `Var "x")), [`Int 21]) in
      let expected =
        of_instrs [ LDC 21 ; LDF "lam_1" ; AP 1 ; RTN ] @
        L "lam_1" ::
        of_instrs [ LD (0, 0) ; LD (0, 0) ; ADD ; RTN ]
      in
      let compiled = compile input in
      assert_equal ~printer:dump expected compiled
    end;
    "compile stupid_rec" >:: begin fun _ ->
      let input : sour = `LetRec ([`Fun ("sub", ["x";"y"], `Binop (Sub, `Var "x", `Var "y"))],
                                   `App (`Var "sub", [ `Int 3; `Int 2 ])) in
      let expected =
        of_instrs [ DUM 1 ; LDF "body_sub_1" ; LDF "rec_entry_1_2" ; RAP 1 ; RTN ] @
        L "body_sub_1" ::
        of_instrs [ LD (0,0) ; LD (0,1) ; SUB ; RTN ] @
        L "rec_entry_1_2" ::
        of_instrs [   LDC 3 ; LDC 2 ; LD (0,0) ; AP 2 ; RTN ] in
      let compiled = compile input in
      assert_equal ~printer:Egcc.prg_to_str expected compiled;
    end;
(*
    "compile sum_rec" >:: begin fun _ ->
        (* let rec sum = if n>0 then n + sum (n-1) else 0 *)
      let input : sour =
        `LamRec
        `App (`Fix ("sum", ["n"],
                              `If (`Var "n",
                                `Binop (Add, `Var "n",
                                  `App (`Var "sum", [`Binop(Sub, `Var "n", `Int 1)])),
                                `Int 0)),
                            [ `Int 5 ]) in
      let expected = [
            I (LDC 5);
            I (LDF "rlam_sum_1");
            I (AP 1);
            I (RTN);
            L ("entry_sum_1");
            I (LDF "body_sum_1");
            I (AP 1);
            I (RTN);
            L ("if_t_2");
            I (LD (0,0));
            I (LD (0,0));
            I (LDC 1);
            I (SUB);
            I (LD (1,0));
            I (AP 1);
            I (ADD);
            I (JOIN);
            L ("rlam_sum_1");
            I (LD (0,0));
            I (DUM 1);
            I (LDF "body_sum_1");
            I (LDF "entry_sum_1");
            I (RAP 1);
            I (RTN);
            L ("if_f_2");
            I (LDC 0);
            I (JOIN);
            L ("body_sum_1");
            I (LD (0,0));
            I (SEL ("if_t_2","if_f_2"));
            I (RTN);
          ] in
      let expected_low = [
        LDC 5;
        LDF 15;
        AP 1;
        RTN;
        LDF 23;
        AP 1;
        RTN;
        LD (0,0);
        LD (0,0);
        LDC 1;
        SUB;
        LD (1,0);
        AP 1;
        ADD;
        JOIN;
        LD (0,0);
        DUM 1;
        LDF 23;
        LDF 4;
        RAP 1;
        RTN;
        LDC 0;
        JOIN;
        LD (0,0);
        SEL (7,21);
        RTN
      ] in
      let compiled = compile input in
      let compiled_low = Egcc.compile compiled in
      assert_equal ~printer:Egcc.prg_to_str expected compiled;
      assert_equal ~printer:Gcc.prg_to_str expected_low compiled_low
    end;
 *)
  ]
