open Batteries
open Sour

type 'a sweet' = [ 'a sour'
                 | `Let of 'a Sour.decl' list * 'a
                 | `Inline of string * 'a * 'a ]

type sweet = sweet sweet'

let rec desugar: sweet -> sour =
  function
  (* new cases *)
  | `Inline (x, e, e') -> failwith "TODO: inline not implemented"
  | `Let (decls, e) ->
      let (vl, el) =
        decls
        |> List.map (function
                     | `Const (x, e') -> (x, e')
                     | `Fun (x, xs, e') -> (x, `Lam (xs, e')))
        |> List.split
      in
      `App( `Lam( vl, desugar e ), List.map desugar el )
  (* old cases *)
  | `Int i -> `Int i
  | `Var s -> `Var s
  | `Binop(b,e1,e2) -> `Binop(b,desugar e1, desugar e2)
  | `Unop(u,e) -> `Unop(u,desugar e)
  | `If(e1,e2,e3) -> `If(desugar e1, desugar e2, desugar e3)
  | `App(e,el) -> `App(desugar e, List.map desugar el)
  | `Lam(sl,e) -> `Lam(sl, desugar e)
  | `LetRec(decls,e) -> `LetRec(
      List.map (function `Fun (x, xs, e') -> `Fun (x, xs, desugar e')
                       | `Const (x, e') -> `Const (x, desugar e')) decls,
      desugar e)
  | `Dbug(e1,e2) -> `Dbug(desugar e1, desugar e2)
  | `Brk(e) -> `Brk(desugar e)

let list_to_cons ls : sweet =
  List.fold_right (fun l cons -> `Binop (Cons, l, cons)) ls (`Int 0)

let tuple2_to_cons (a1,a2) : sweet = `Binop(Cons, a1, a2)
let tuple3_to_cons (a1,a2,a3) : sweet = `Binop(Cons, a1, tuple2_to_cons (a2,a3))
let tuple4_to_cons (a1,a2,a3,a4) : sweet = `Binop(Cons, a1, tuple3_to_cons (a2,a3,a4))
let tuple5_to_cons (a1,a2,a3,a4,a5) : sweet = `Binop(Cons, a1, tuple4_to_cons (a2,a3,a4,a5))

let spr = Printf.sprintf

let rec string_of_sweet : sweet -> string = function
  | `Inline (x, e, e') -> failwith "TODO: inline not implemented"
  | `Let (defs, e) ->
    let sdefs =
      defs |> List.map (function
                        | `Const (var, body) ->
                            spr "%s = %s" var (string_of_sweet body)
                        | `Fun (var, params, body) ->
                            spr "%s %s = %s" var
                                            (String.concat " " params)
                                            (string_of_sweet body))
    in
    let se = string_of_sweet e in
    spr "\nlet %s in\n%s" (String.concat "\nand " sdefs) se
  | `Int i -> string_of_int i
  | `Var s -> s
  | `Binop(b,e1,e2) ->
    let se1 = string_of_sweet e1 in
    let se2 = string_of_sweet e2 in
    let sb = bin_to_str b in
      spr "(%s %s %s)" se1 sb se2
  | `Unop(u,e) ->
    let se = string_of_sweet e in
    let su = un_to_str u in
      spr "%s( %s )" su se
  | `If(e1,e2,e3) ->
    let se1 = string_of_sweet e1 in
    let se2 = string_of_sweet e2 in
    let se3 = string_of_sweet e3 in
      spr "if %s then\n  %s else\n  %s\n" se1 se2 se3
  | `App(e,el) ->
    let se = string_of_sweet e in
    let sel = String.concat ", " (List.map string_of_sweet el) in
      spr "( %s ) ( %s )\n" se sel
  | `Lam(sl,e) ->
    let se = string_of_sweet e in
    let ssl = String.concat " " sl in
      spr "fun %s -> %s\n" ssl se
  | `LetRec (defs, e) ->
    let sdefs =
      defs |> List.map (function
                        | `Const (var, body) ->
                            spr "%s = %s" var (string_of_sweet body)
                        | `Fun (var, params, body) ->
                            spr "%s %s = %s" var
                                            (String.concat " " params)
                                            (string_of_sweet body))
    in
    let se = string_of_sweet e in
    spr "\nlet rec %s in\n%s" (String.concat "\nand " sdefs) se
  | `Dbug(e1,e2) ->
    let se1 = string_of_sweet e1 in
    let se2 = string_of_sweet e2 in
      spr "debug %s %s\n" se1 se2
  | `Brk(e) ->
    let se = string_of_sweet e in
      spr "break %s\n" se

let string_of_sour e = string_of_sweet (e : sour :> sweet)

let rec sweet_data_to_sim = function
  | `Binop (Cons, a, b) -> Sim.Pair (sweet_data_to_sim a, sweet_data_to_sim b)
  | `Int i -> Sim.Int i
  | _ -> failwith "Not valid stack data data in sweet to sim data conversion"
