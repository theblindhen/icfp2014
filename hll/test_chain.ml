(* Some tests for the whole chain from Sweet to Simulator *)
open Batteries

let unittest () =
  let open OUnit2 in
  let verify_output prg_str output =
    let prg = 
      let lexbuf = Lexing.from_string prg_str in
      Hlparser.prg Hllexer.token lexbuf in
    let gcc = prg |> Sweet.desugar |> Sour.compile |> Egcc.compile |> Array.of_list in
    assert_equal ~printer:dump (fst (Sim.run None false gcc)) output
  in
  "Test Chain" >::: [
    "test_fun1" >:: begin fun _ ->
      verify_output "
        let a x y = x - y
        in a (5,3)
        " (Sim.Int 2)
    end;
    "test_fun2" >:: begin fun _ ->
      verify_output "
        let a x y = x - y
        and b x y = x / y
        and z = 5
        in b (z, a (z,3))
        " (Sim.Int 2)
    end;
    "test_letrec1" >:: begin fun _ ->
      verify_output "
        let rec sum n = if n then n + sum (n-1) else 0
        in sum (5)
        " (Sim.Int 15)
    end;
    "test_letrec2" >:: begin fun _ ->
      verify_output "
        let rec a n = 2 + b (n-1)
        and y = 3
        and b n = if n > 1 then a (n-1) else y
        in  a (3)
        " (Sim.Int 7)
    end;
    "test_letrec3" >:: begin fun _ ->
      verify_output "
        let rec a n = 2 + b (n-1)
        and y = 3
        and x = b (2)
        and b n = if n > 1 then a (n-1) else y
        in  (x*100) + a (3)
        " (Sim.Int 507)
    end;
  ]
