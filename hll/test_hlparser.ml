open Batteries
open OUnit2

let lexparse str =
  Lexing.from_string str
  |> Hlparser.prg Hllexer.token

let assert_parses str expected =
  let result = lexparse str in
  assert_equal ~msg:"Program misparsed" ~printer:Sweet.string_of_sweet
    expected result;
  let reparsed = Sweet.string_of_sweet result |> lexparse in
  assert_equal ~msg:"Program misprinted" expected reparsed

let unittest () =
  "HLL parser" >::: [
    "Parse subtraction" >:: begin fun _ ->
      assert_parses "(fun x y -> x - y)(5, 3)"
        Sour.(`App (
          `Lam (["x";"y"], `Binop (Sub, `Var "x", `Var "y")),
          [`Int 5; `Int 3]
        ))
    end;
    "Associativity of -" >:: begin fun _ ->
      assert_parses "1 - 2 - 3"
        Sour.(`Binop (Sub, `Binop (Sub, `Int 1, `Int 2), `Int 3))
    end;
    "Precedence of + and *" >:: begin fun _ ->
      assert_parses "1*2 + 3*4"
        Sour.(`Binop (Add, `Binop (Mul, `Int 1, `Int 2), `Binop (Mul, `Int 3, `Int 4)))
    end;

    "Precedence of unop" >:: begin fun _ ->
      assert_parses "atom car cdr [1]"
        Sour.(`Unop (Atom, `Unop (Car, `Unop (Cdr, `Binop (Cons, `Int 1, `Int 0)))))
    end;

    "Precedence of unop and binop combination" >:: begin fun _ ->
      assert_parses "car [1] + cdr (1,2)"
        Sour.(`Binop (Add, 
          `Unop (Car, `Binop (Cons, `Int 1, `Int 0)),
          `Unop (Cdr, `Binop (Cons, `Int 1, `Int 2))))
    end;

    "Precedence of unop and binop combination" >:: begin fun _ ->
      assert_parses "car cdr [1,2] + cdr (1,2) * 5"
        Sour.(`Binop (Add, 
          `Unop (Car, `Unop (Cdr, `Binop (Cons, `Int 1, `Binop (Cons, `Int 2, `Int 0)))),
          `Binop(Mul, `Unop (Cdr, `Binop (Cons, `Int 1, `Int 2)), `Int 5)))
    end;

    "Parse simple list" >:: begin fun _ ->
        assert_parses "[a,b,c]"
          Sour.(
            `Binop (Cons, `Var "a",
              `Binop (Cons, `Var "b",
                `Binop (Cons, `Var "c", `Int 0))))
      end;
    "Parse simple tuple" >:: begin fun _ ->
        assert_parses "(a,b,c,d)"
          Sour.(
            `Binop (Cons, `Var "a",
              `Binop (Cons, `Var "b",
                `Binop (Cons, `Var "c", `Var "d"))))
      end;
    "Parse simple tuple" >:: begin fun _ ->
        assert_parses "(a,[b,(c,d)])"
          Sour.(
            `Binop (Cons, `Var "a",
              `Binop (Cons, `Var "b",
                `Binop (Cons,
                   `Binop (Cons, `Var "c", `Var "d"),
                   `Int 0))))
      end;
    "Parse function app to tuple" >:: begin fun _ ->
        assert_parses "f((a,b))"
          Sour.(
            `App (`Var "f",
              [`Binop (Cons, `Var "a", `Var "b")]))
      end;
    "Parse function with multiple arguments" >:: begin fun _ ->
        assert_parses "f(a,b)"
          Sour.(
            `App (`Var "f",
              [`Var "a"; `Var "b"]))
      end;
    "Parse function curried" >:: begin fun _ ->
        assert_parses "f(a)(b)"
          Sour.(
            `App(
               `App (`Var "f", [ `Var "a" ]),
               [ `Var "b" ]))
    end;
    "Parse let without and" >:: begin fun _ ->
        assert_parses "let x = 1 in x + x"
          Sour.(
            `Let ([`Const ("x", `Int 1)],`Binop (Add, `Var "x", `Var "x")))
    end;
    "Parse let with and" >:: begin fun _ ->
        assert_parses "let x = 1 and y = 2 in x + y"
          Sour.(
            `Let ([`Const ("x", `Int 1);
                   `Const ("y", `Int 2)],
                  `Binop (Add, `Var "x", `Var "y")))
    end;
    "Parse factorial" >:: begin fun _ ->
      assert_parses "let rec fib x = if x then x * fib(x-1) else 1 in fib(3)"
        Sour.(`LetRec ([
          `Fun ("fib", ["x"],
                `If (`Var "x",
                     `Binop (Mul,
                             `Var "x",
                             `App(`Var "fib", [`Binop (Sub, `Var "x", `Int 1)])),
                     `Int 1)
          )],
          `App (`Var "fib", [`Int 3])
        ))
    end;
  ]
