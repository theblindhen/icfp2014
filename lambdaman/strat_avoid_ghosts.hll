// local strategy with variables in scope:
// - local state: not required
// - gamelogic functions and values
// - basic functions
(
#ifndef MAX_DIST_OF_GHOST
# define MAX_DIST_OF_GHOST 6
#endif

#ifndef PROXIMITY_MULT
# define PROXIMITY_MULT 50
#endif

#define LM_VITAL_MULT ~1

// init: return the initial private state for this strategy
fun dummy -> 0         //no state
,
// update: return updated private state based on the move taken
fun dir ->
0
,
// step: weighted dirs are (dir * score) list, adjust or filter this
fun curr_weighted_dirs ->
  let update_weights weighted_dirs gh_status =
    let gh_pos = pi2 (gh_status)
    and gh_dir = tuple_last3(gh_status) in 
    //NOTE: ghost_reachable: returns (no_of_steps,(x,y))
    let gh_reachs = ghost_reachable(state, MAX_DIST_OF_GHOST, gh_pos, gh_dir) in
    map (fun wd ->
      let pos = pos_after_move(current_pos, fst wd) in
      let index = findi (fun y -> tuple_cmp(snd y,pos), gh_reachs) in
      if index = ~1 then 
        wd 
      else
        let gh_reach = nth(gh_reachs, index) in
          if lm_vital > fst gh_reach then
            (fst wd, snd wd - LM_VITAL_MULT * PROXIMITY_MULT * (MAX_DIST_OF_GHOST - fst gh_reach))
          else 
            (fst wd, snd wd - PROXIMITY_MULT * (MAX_DIST_OF_GHOST - fst gh_reach))      ,
      weighted_dirs
    )    
  in               
  fold_left((fun wd gh -> update_weights(wd,gh)), curr_weighted_dirs, ghost_statuses)
)
