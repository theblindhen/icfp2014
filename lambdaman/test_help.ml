open Batteries

open Sweet

(*               ASCII          Spec of static map during game
  Empty          <space> 	1
  Wall           # 	        0
  Pill           . 	        2
  Power Pill     o 	        3
  Fruit          % 	        4
  Lambda-Man     \ 	        5  (Lambda-Man starting position)
  Ghost          = 	        6  (Ghost starting position)
*)

let build_world map_string : sweet =
  let map_lines = String.nsplit map_string "\n" in
  let _ =
    let n = String.length (List.hd map_lines) in
    List.iteri (fun i s ->
      if String.length s != n then
        failwith (Printf.sprintf "Line %d not of length %d" i n)
      else ()) map_lines in
  let _ =
    let map_nwall = Hashtbl.create 10 in
    List.iteri (fun i line -> 
      List.iteri (fun j c -> 
	let not_wall = match c with| '#' -> false | _ -> true in
          Hashtbl.add map_nwall (i, j) not_wall
      ) (String.explode line)) map_lines;
    Hashtbl.iter (fun (i, j) not_wall -> 
      if not_wall then
        try
	  if (Hashtbl.find map_nwall (i + 1, j)) && 
	     (Hashtbl.find map_nwall (i, j + 1)) && 
             (Hashtbl.find map_nwall (i + 1, j + 1)) then
            failwith "not a well-formed map, it contains a 2x2 area without a wall"
          else ()
        with Not_found -> ()
      else ()) map_nwall in
  let static_map : sweet =
    let convert_line line = line |> String.explode
                                 |> List.map (function
                                               | ' ' -> `Int 1
                                               | '#' -> `Int 0
                                               | '.' -> `Int 2
                                               | 'o' -> `Int 3
                                               | '%' -> `Int 4
                                               | '\\'-> `Int 5
                                               | '=' -> `Int 6
                                               | _ -> raise (Failure "Unsupported map char")
                                             )
                                 |> list_to_cons in
    list_to_cons (List.map convert_line map_lines) in
  let find_all_pos ch =
    let rec  _lines y = function
    | [] -> []
    | line::rem_map ->
       let xs = String.explode line
                |> List.filteri_map (fun i ch' -> if ch=ch' then Some i else None)
       in List.map (fun x -> `Binop (Sour.Cons, `Int x,`Int y)) xs @ _lines (y+1) rem_map
    in
    _lines 0 map_lines in
  let lambda_man : sweet = tuple5_to_cons(
     `Int 0, (*vitality*)
     List.hd (find_all_pos '\\'), (* initial pos *)
     `Int 0, (* direction *)
     `Int 3, (* no. lives *)
     `Int 0) (* score *) in
  let ghosts : sweet = list_to_cons (List.map (fun pos -> tuple3_to_cons (`Int 0, pos, `Int 0)) (find_all_pos '=')) in
  let fruit = `Int 0 in
  tuple4_to_cons (static_map, lambda_man, ghosts, fruit)



let sim_prg_in_world world state_expr_str expr_str =
  (* Simulate expr_str in an environment with world, a specific state_expr and basics/gamelogic *)
  let prg = 
    (* include basics *)
    let basics_prg =
      let str = "let rec dummy=42 "^ input_file "lambdaman/basics.hll" ^" in 0" in
      let lexbuf = Lexing.from_string str in
      Hlparser.prg Hllexer.token lexbuf in
    (* include gamelogic and setup quick map and state *)
    let test_prg =
      let raw_prg = "
         let rec dummy=21 \n"
       ^" and s_quick_map = init_quick_map(world) \n"
       ^" and s_quick_map_set state = 0 \n"
       ^ "#include \"lambdaman/gamelogic.hll\"\n"
       ^" and state = "^ state_expr_str^"\n"
       ^" in "
       ^ expr_str in
      let preprocessed_prg = Common.cpp_string raw_prg in
      let lexbuf = Lexing.from_string preprocessed_prg in
      Hlparser.prg Hllexer.token lexbuf in
    (* put the world around everything *)
    match basics_prg with
    | `LetRec (decls,_) -> 
       `LetRec (decls,`Let ([`Const ("world", world)], test_prg))
    | _ -> failwith "Strange?!?"
  in
  prg |> Sweet.desugar |> Sour.compile |> Egcc.compile |> Array.of_list |> Sim.run None false |> fst
  
