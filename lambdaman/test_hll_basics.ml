(*  Tests for basics.hll  *)
open Batteries

let unittest () =
  let open OUnit2 in
  let verify_output prg_str output =
    let lib = input_file "lambdaman/basics.hll" in
    let prg = 
      let lexbuf = Lexing.from_string ("let rec dummy=42 "^ lib ^" in "^ prg_str) in
      Hlparser.prg Hllexer.token lexbuf in
    let gcc = prg |> Sweet.desugar |> Sour.compile |> Egcc.compile |> Array.of_list in
    assert_equal ~printer:dump output (fst (Sim.run None false gcc))
  in
  let sim_int_list xs =
    List.fold_right (fun x acc -> Sim.Pair (Sim.Int x, acc)) xs (Sim.Int 0)
  in
  let sim_int_queue hd_lst tl_lst count =
    Sim.Pair ( sim_int_list(hd_lst), Sim.Pair( sim_int_list(tl_lst), Sim.Int count ) )
  in
  let sim_int_pair_list xs =
    List.fold_right (fun x acc -> Sim.Pair (Sim.Pair (Sim.Int (fst x), Sim.Int (snd x)), acc)) xs (Sim.Int 0)
  in
  "Test Chain" >::: [
    "test_map" >:: begin fun _ ->
      verify_output "map((fun x -> x * 2), [1,2,3])" (sim_int_list [2;4;6])
    end;
    "test_append" >:: begin fun _ ->
      verify_output "append([1,2], [3])" (sim_int_list [1;2;3])
    end;
    "test_flatten" >:: begin fun _ ->
      verify_output "flatten([[1,2], [3], [5,6]])" (sim_int_list [1;2;3;5;6])
    end;
    "test_fold_left" >:: begin fun _ ->
      verify_output "fold_left((fun acc x -> acc - x), 400, [100,30,8,1])"
        (Sim.Int (List.fold_left (-) 400 [100;30;8;1]))
    end;
    "test_fold_right" >:: begin fun _ ->
      verify_output "fold_right((fun x acc -> x - acc), [100,30,8,1], 400)"
        (Sim.Int (List.fold_right (-) [100;30;8;1] 400))
    end;
    "test_rev" >:: begin fun _ ->
      verify_output "rev([1, 2, 3])" (sim_int_list [3;2;1])
    end;
    "test_rev" >:: begin fun _ ->
      verify_output "rev([1])" (sim_int_list [1])
    end;
    "test_rev" >:: begin fun _ ->
      verify_output "rev([])" (sim_int_list [])
    end;
    "test_not" >:: begin fun _ ->
      verify_output "not (2)" (Sim.Int 0)
    end;
    "test_not2" >:: begin fun _ ->
      verify_output "not (0)" (Sim.Int 1)
    end;
    "test_list_empty" >:: begin fun _ ->
      verify_output "list_empty ([])" (Sim.Int 1)
    end;
    "test_list_empty2" >:: begin fun _ ->
      verify_output "list_empty ([ 1 , 2 ])" (Sim.Int 0)
    end;
    "test_list_empty3" >:: begin fun _ ->
      verify_output "list_empty (snd ([ 2 ]))" (Sim.Int 1)
    end;
    "test_list_empty4" >:: begin fun _ ->
      verify_output "list_empty (snd ([ 1 , 2 ]))" (Sim.Int 0)
    end;
    (* pi tests *)
    "test_pi" >:: begin fun _ ->
      verify_output "pi1 ((1,2))" (Sim.Int 1);
      verify_output "pi1 ([1,2])" (Sim.Int 1);
      verify_output "pi1 ([1])" (Sim.Int 1)
    end;
    "test_pi2" >:: begin fun _ ->
      verify_output "pi2 ((1,2, 3))" (Sim.Int 2);
      verify_output "pi2 ([1,2])" (Sim.Int 2);
      verify_output "pi2 ([1,2,3])" (Sim.Int 2)
    end;
    "test_tuple_last" >:: begin fun _ ->
      verify_output "tuple_last2 ((1, 2))" (Sim.Int 2);
      verify_output "tuple_last3 ((1,2, 3))" (Sim.Int 3);
      verify_output "tuple_last4 ((1,2, 3,4))" (Sim.Int 4);
    end;
    "test_tuple_cmp" >:: begin fun _ ->
      verify_output "tuple_cmp ((1,2),(1,2))" (Sim.Int 1);
      verify_output "tuple_cmp ((1,2),(1,3))" (Sim.Int 0);
      verify_output "tuple_cmp ((1,2),(3,2))" (Sim.Int 0);
    end;
    "test_nth" >:: begin fun _ ->
      verify_output "nth ([1,2,3], 0)" (Sim.Int 1);
      verify_output "nth ([1,2,3], 1)" (Sim.Int 2);
      verify_output "nth ([1,(2,3),4], 1)" (Sim.Pair (Sim.Int 2, Sim.Int 3));
      verify_output "nth (((1,2, 3)), 1)" (Sim.Int 2);
      verify_output "nth ((1,2), 0)" (Sim.Int 1);
    end;
    "test_enum_set" >:: begin fun _ ->
      verify_output "enum_set1 ((1, 2), 9)" (Sim.Pair (Sim.Int 9, Sim.Int 2));
      verify_output "enum_set2 ((1, 2, 3), 9)" (Sim.Pair (Sim.Int 1, Sim.Pair (Sim.Int 9, Sim.Int 3)));
      verify_output "enum_set3 ((1, 2, 3, 4), 9)"
                    (Sim.Pair (Sim.Int 1, Sim.Pair (Sim.Int 2, Sim.Pair (Sim.Int 9, Sim.Int 4))));
      verify_output "enum_set4 ((1, 2, 3, 4, 5), 9)"
                    (Sim.Pair (Sim.Int 1, Sim.Pair (Sim.Int 2, Sim.Pair (Sim.Int 3, Sim.Pair (Sim.Int 9, Sim.Int 5)))));
      verify_output "enum_set5 ((1, 2, 3, 4, 5, 6), 9)"
                    (Sim.Pair (Sim.Int 1, Sim.Pair (Sim.Int 2, Sim.Pair (Sim.Int 3, Sim.Pair (Sim.Int 4, Sim.Pair (Sim.Int 9, Sim.Int 6))))));
      verify_output "enum_set1 ([1, 2, 3], 9)"
                    (Sim.Pair (Sim.Int 9, Sim.Pair (Sim.Int 2, Sim.Pair (Sim.Int 3, Sim.Int 0))));
      verify_output "enum_set2 ([1, 2, 3], 9)"
                    (Sim.Pair (Sim.Int 1, Sim.Pair (Sim.Int 9, Sim.Pair (Sim.Int 3, Sim.Int 0))));
      verify_output "enum_set3 ([1, 2, 3], 9)"
                    (Sim.Pair (Sim.Int 1, Sim.Pair (Sim.Int 2, Sim.Pair (Sim.Int 9, Sim.Int 0))));
    end;
    "test_findi" >:: begin fun _ ->
      verify_output "findi (id, [0,1,2,3])" (Sim.Int 1);
      verify_output "findi (id, [0,0,0,1])" (Sim.Int 3);
      verify_output "findi (fun t -> not (atom(t)), [0,(1,2),0,1])" (Sim.Int 1);
    end;
    "test_mem" >:: begin fun _ ->
      verify_output "mem (fun x y -> x = y, 1, [0,1,2,3])" (Sim.Int 1);
      verify_output "mem (fun x y -> x = y, 5, [0,1,2,3])" (Sim.Int 0);
      verify_output "mem (fun x y -> fst(x) = fst(y), (42, 3), [(42, 2), (0, 1), (0, 2), (1, 3)])" (Sim.Int 1);
    end;
    "test_min" >:: begin fun _ ->
      verify_output "min (3, 4)" (Sim.Int 3);
      verify_output "min (4, 4)" (Sim.Int 4);
      verify_output "min (4, 3)" (Sim.Int 3);
    end;
    "test_max" >:: begin fun _ ->
      verify_output "max (3, 4)" (Sim.Int 4);
      verify_output "max (3, 3)" (Sim.Int 3);
      verify_output "max (4, 3)" (Sim.Int 4);
    end;
    "test_min" >:: begin fun _ ->
      verify_output "abs (42)" (Sim.Int 42);
      verify_output "abs (~42)" (Sim.Int 42);
    end;
    "test_length" >:: begin fun _ ->
      verify_output "length ([4,3,2,1])" (Sim.Int 4);
      verify_output "length ([])" (Sim.Int 0);
    end;
    "test_index_of" >:: begin fun _ ->
      verify_output "index_of ([4,3,2,1], 3)" (Sim.Int 1);
      verify_output "index_of ([4,3,2,1], 1)" (Sim.Int 3);
      verify_output "index_of ([4,3,2,1], 4)" (Sim.Int 0);
      verify_output "index_of ([4,3,2,1], 0)" (Sim.Int (-1));
      verify_output "index_of ([4,3,2,1], 5)" (Sim.Int (-1));
    end;
    "test_index_of" >:: begin fun _ ->
      verify_output "filter (fun x -> x > 3, [4,3,2,1])" (Sim.Pair (Sim.Int 4, Sim.Int 0));
      verify_output "filter (fun x -> x > 2, [])" (Sim.Int 0);
      verify_output "filter (fun x -> x > 10, [4,3,2,1])" (Sim.Int 0);
    end;
    "test_add_vec" >:: begin fun _ ->
      verify_output "add_vec ((2, 3), (4, 5))" (Sim.Pair (Sim.Int 6, Sim.Int 8));
    end;
    "test_cmp_vec1" >:: begin fun _ ->
      verify_output "cmp_vec ((2, 3), (2, 5))" (Sim.Int 0);
    end;
    "test_cmp_vec2" >:: begin fun _ ->
      verify_output "cmp_vec ((2, 3), (2, 3))" (Sim.Int 1);
    end;
    "test_mod" >:: begin fun _ ->
      verify_output "mod (3, 4)" (Sim.Int 3);
      verify_output "mod (5, 4)" (Sim.Int 1);
      verify_output "mod (5, 5)" (Sim.Int 0);
    end;
    "test_manhattan" >:: begin fun _ ->
      verify_output "manhattan ((3, 4), (3, 4))" (Sim.Int 0);
      verify_output "manhattan ((3, 4), (3, 5))" (Sim.Int 1);
      verify_output "manhattan ((3, 4), (4, 5))" (Sim.Int 2);
    end;
    "test_rem" >:: begin fun _ ->
      verify_output "rem (fun x y -> x = y, 2, [1, 2])" (Sim.Pair (Sim.Int 1, Sim.Int 0));
      verify_output "rem (fun x y -> x = y, 2, [1, 2, 2])" (Sim.Pair (Sim.Int 1, Sim.Int 0));
      verify_output "rem (fun x y -> x = y, 2, [42])" (Sim.Pair (Sim.Int 42, Sim.Int 0));
      verify_output "rem (fun x y -> x = y, 2, [])" (Sim.Int 0);
    end;
    "test_split" >:: begin fun _ ->
      verify_output "split ([1, 2, 3, 4])" (Sim.Pair (sim_int_list [3;1], (sim_int_list [4;2])));
      verify_output "split ([1, 2, 3, 4, 5])" (Sim.Pair (sim_int_list [5;3;1], (sim_int_list [4;2])));
      verify_output "split ([2, 2])" (Sim.Pair (sim_int_list [2], (sim_int_list [2])));
      verify_output "split ([])" (Sim.Pair (sim_int_list [], (sim_int_list [])));
    end;
    "test_find_max" >:: begin fun _ ->
      verify_output "find_max (id, [1, 2, 3, 4])" (Sim.Int 4);
      verify_output "find_max ((fun x -> 0-x), [1, 2, 3, 4])"  (Sim.Int 1);
      verify_output "find_max (id,[1])" (Sim.Int 1);
      verify_output "find_max (pi1, [(1,4), (2,3), (3,2), (4,1)])" (Sim.Pair (Sim.Int 4, Sim.Int 1));
    end;
    "test_merge" >:: begin fun _ ->
      verify_output "merge (fun x y -> x >= y, [3, 3, 1], [5, 4, 2])" (sim_int_list [5;4;3;3;2;1]);
    end;
    "test_sort" >:: begin fun _ ->
      verify_output "sort (fun x y -> x >= y, [4,2,6,7,2,1])" (sim_int_list [7;6;4;2;2;1]);
      verify_output "sort (fun x y -> fst(x) >= fst(y), [(2,1),(3,1),(1,1)])" (sim_int_pair_list [(3,1);(2,1);(1,1)]);
    end;
    "test_uniq" >:: begin fun _ ->
      verify_output "uniq (fun x y -> x >= y, [4,2,7,6,6,7,2,1])" (sim_int_list [7;6;4;2;1]);
    end;
    "test_q_empty" >:: begin fun _ ->
      verify_output "q_empty" (sim_int_queue [] [] 0)
    end;
    "test_q_push1" >:: begin fun _ ->
      verify_output "q_push( 1, q_empty )" (sim_int_queue [1] [] 1)
    end;
    "test_q_push2" >:: begin fun _ ->
      verify_output "q_push( 2, q_push( 1, q_empty ))" (sim_int_queue [2; 1] [] 2)
    end;
    "test_q_pop1" >:: begin fun _ ->
      verify_output "q_pop( q_push( 1, q_empty ) )" 
      (Sim.Pair( Sim.Int 1, sim_int_queue [] [] 0 ))
    end;
    "test_q_pop2" >:: begin fun _ ->
      verify_output "q_pop( q_push( 2, q_push( 1, q_empty ) ) )" 
      (Sim.Pair( Sim.Int 1, sim_int_queue [] [2] 1 ))
    end;
    "test_q_mem1" >:: begin fun _ ->
      verify_output "q_mem(fun x y -> x=y, 1, q_push( 2, q_push( 1, q_empty ) ) )" (Sim.Int 1)
    end;
    "test_q_mem2" >:: begin fun _ ->
      verify_output "q_mem(fun x y -> x=y, 3, q_push( 2, q_push( 1, q_empty ) ) )" (Sim.Int 0)
    end;  
    "test_q_to_array1" >:: begin fun _ ->
      verify_output "q_to_array( q_push(3,q_push(2,q_push(1,q_empty))), 1 )" 
      ( 
        Sim.Pair(
          Sim.Pair( Sim.Pair( Sim.Int 1, Sim.Int 2 ), Sim.Pair( Sim.Int 3, Sim.Int 0 ) ),
          Sim.Int 4
        )
      )
    end;
    "test_q_to_array2" >:: begin fun _ ->
      verify_output "q_to_array( q_push(5,q_push(4,q_push(3,q_push(2,q_push(1,q_push(0,q_empty)))))) )" 
      (
        Sim.Pair(
          Sim.Pair( 
            Sim.Pair( 
              Sim.Pair( Sim.Int 0, Sim.Int 1 ), 
              Sim.Pair( Sim.Int 2, Sim.Int 3 ) 
            ),
            Sim.Pair( 
              Sim.Pair( Sim.Int 4, Sim.Int 5 ), 
              Sim.Int 0
            )
          ),
          Sim.Int 8
        )
      )
    end;
    "test_a_nth1" >:: begin fun _ ->
      verify_output "a_nth(q_to_array( q_push(5,q_push(4,q_push(3,q_push(2,q_push(1,q_push(0,q_empty)))))) ),5) " 
      (Sim.Int 5)
    end;    
    "test_a_nth1" >:: begin fun _ ->
      verify_output "a_nth(q_to_array( q_push(5,q_push(4,q_push(3,q_push(2,q_push(1,q_push(0,q_empty)))))) ),4) " 
      (Sim.Int 4)
    end;    
    "test_a_nth2" >:: begin fun _ ->
      verify_output "a_nth(q_to_array( q_push(5,q_push(4,q_push(3,q_push(2,q_push(1,q_push(0,q_empty)))))) ),1) " 
      (Sim.Int 1)
    end;]
