(*  Tests for gamelogic.hll  *)
open Batteries

let std_world = Test_help.build_world
"########
#..#...#
#.#= #.#
#.####.#
#o.....#
##.##.##
#....%.#
#.####.#
#..\\...#
########"

let unittest () =
  let open OUnit2 in
  let verify_output world test_str expected =
    let result = Test_help.sim_prg_in_world world "0" test_str in
    assert_equal ~printer:dump expected result
  in
  let sim_int_list xs =
    List.fold_right (fun x acc -> Sim.Pair (Sim.Int x, acc)) xs (Sim.Int 0) in
  let sim_3_tuple_list xs =
    List.fold_right (fun x acc -> Sim.Pair (Sim.Pair (Sim.Int (fst x), Sim.Pair (Sim.Int (fst(snd x)), Sim.Int (snd (snd x)))), acc)) xs (Sim.Int 0) in
  "Test Chain" >::: [
    "get_map" >:: begin fun _ ->
      let map = match std_world with
                | `Binop (Sour.Cons, m, _) -> m
                | _ -> failwith "Invalid world"
      in
      verify_output std_world "static_map" (Sweet.sweet_data_to_sim map)
    end;
    "get_lm_start_pos" >:: begin fun _ ->
      verify_output std_world "get_lm_start_pos (world)" (Sim.Pair (Sim.Int 3, Sim.Int 8))
    end;
    "get_fruit_start_pos" >:: begin fun _ ->
      verify_output std_world "get_fruit_start_pos (world)" (Sim.Pair (Sim.Int 5, Sim.Int 6))
    end;
    "current_pos" >:: begin fun _ ->
      verify_output std_world "current_pos" (Sim.Pair (Sim.Int 3, Sim.Int 8))
    end;
    "current_dir" >:: begin fun _ ->
      verify_output std_world "current_dir" (Sim.Int 0)
    end;
    "opposite_dir" >:: begin fun _ ->
      verify_output std_world "opposite_dir (0)" (Sim.Int 2);
      verify_output std_world "opposite_dir (1)" (Sim.Int 3);
      verify_output std_world "opposite_dir (2)" (Sim.Int 0);
      verify_output std_world "opposite_dir (3)" (Sim.Int 1)
    end;
    "pos_after_move" >:: begin fun _ ->
      verify_output std_world "pos_after_move ((3,8), 0)" (Sim.Pair (Sim.Int 3, Sim.Int 7));
      verify_output std_world "pos_after_move ((3,8), 1)" (Sim.Pair (Sim.Int 4, Sim.Int 8));
      verify_output std_world "pos_after_move ((3,8), 2)" (Sim.Pair (Sim.Int 3, Sim.Int 9));
      verify_output std_world "pos_after_move ((3,8), 3)" (Sim.Pair (Sim.Int 2, Sim.Int 8))
    end;
    "fruit_is_here" >:: begin fun _ ->
      verify_output std_world "fruit_is_here" (Sim.Int 0);
    end;
    "pos_possible_dir" >:: begin fun _ ->
      verify_output std_world "possible_dir (state, (4, 4))" (sim_int_list [1; 3]);
      verify_output std_world "possible_dir (state, (5, 4))" (sim_int_list [1; 2; 3]); 
    end;
    "pos_ghost_reachable" >:: begin fun _ ->
      verify_output std_world "ghost_reachable (state, 1, (4, 4), 1)" (sim_3_tuple_list [(1, (5, 4))]);
      verify_output std_world "ghost_reachable (state, 2, (4, 4), 1)" (sim_3_tuple_list [(1, (5, 4)); (2, (6, 4)); (2, (5, 5))]);
      verify_output std_world "ghost_reachable (state, 3, (4, 4), 1)" (sim_3_tuple_list [(1, (5, 4)); (2, (6, 4)); (2, (5, 5)); (3, (6, 3)); (3, (5, 6))]);
    end;

    (* Tests for quick_map *)
    "init_quick_map" >:: begin fun _ ->
      (* make sure that init_quick_map can be called *)
      verify_output std_world "let a = init_quick_map (world) in 0" (Sim.Int 0);
    end;
    "lookup_pos_static" >:: begin fun _ ->
      verify_output std_world "lookup_pos_static ((7,8))" (Sim.Int 0);
      verify_output std_world "lookup_pos_static ((4,2))" (Sim.Int 1);
      verify_output std_world "lookup_pos_static ((1,1))" (Sim.Int 2);
      verify_output std_world "lookup_pos_static ((1,4))" (Sim.Int 3);
      verify_output std_world "lookup_pos_static ((5,6))" (Sim.Int 4);
      verify_output std_world "lookup_pos_static ((3,8))" (Sim.Int 5);
      verify_output std_world "lookup_pos_static ((3,2))" (Sim.Int 6);
    end;
    "pos_is_free" >:: begin fun _ ->
      verify_output std_world "pos_is_free ((4,1))" (Sim.Int 1);
      verify_output std_world "pos_is_free ((1,4))" (Sim.Int 1);
      verify_output std_world "pos_is_free ((3,3))" (Sim.Int 0);
    end;
    "pos_is_yummi" >:: begin fun _ ->
      verify_output std_world "pos_is_yummi ((4,1))" (Sim.Int 1);
      verify_output std_world "pos_is_yummi ((1,4))" (Sim.Int 1);
      verify_output std_world "pos_is_yummi ((3,3))" (Sim.Int 0);
      verify_output std_world "pos_is_yummi ((5,6))" (Sim.Int 0); (* no fruit now *)
    end;
    "test_quick_map1" >:: begin fun _ ->
      verify_output std_world 
      "a_nth( gen_quick_map( (fun x y -> (x,y)), [[0,1],[2,3]] ), 3)" 
      (Sim.Pair( Sim.Pair (Sim.Int 1, Sim.Int 1), Sim.Int 3))
    end;
    "test_quick_map2" >:: begin fun _ ->
      verify_output std_world 
      "a_nth( gen_quick_map( (fun x y -> (x,y)), [[0,1,2],[3,4,5],[6,7,8]] ), 5)" 
      (Sim.Pair( Sim.Pair (Sim.Int 2, Sim.Int 1), Sim.Int 5))
    end;
    "test_gen_quick_map3" >:: begin fun _ ->
      verify_output std_world 
      "a_nth( gen_quick_map( (fun x y -> (x,y)), [[0,1,2],[3,4,5],[6,7,8]] ), 4)" 
      (Sim.Pair( Sim.Pair (Sim.Int 1, Sim.Int 1), Sim.Int 4))
    end;
    "test_gen_quick_map4" >:: begin fun _ ->
      verify_output std_world 
      "a_nth( gen_quick_map( (fun x y -> (x,y)), static_map ), 1 + 4 * map_width)" 
      (Sim.Pair( Sim.Pair (Sim.Int 1, Sim.Int 4), Sim.Int 3))
    end;
  ]
