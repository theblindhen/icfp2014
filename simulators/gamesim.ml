open Batteries
open Game
open Sim
open Ghcsim

let sim_int_list xs =
  List.fold_right (fun x acc -> Pair (Int x, acc)) xs (Int 0)

let sim_val_tuple xs =
  let f x acc = match acc with Some acc -> Some (Pair (x, acc)) | None -> Some x in
  match List.fold_right f xs None with
  | Some x -> x
  | None -> failwith "0-tuples not supported"

let sim_val_list xs = List.fold_right (fun x acc -> Pair (x, acc)) xs (Int 0)

let encode_pos (x, y) = 
  Pair (Int x, Int y)

let encode_map map size =
  let rows = ref (Int 0) in
    for i = ((snd size) - 1) downto 0 do
      let col = ref (Int 0) in
        for j = ((fst size) - 1) downto 0 do
	  col := Pair (Int (encode_cell (Hashtbl.find map (j, i))), !col)
	done;
	rows := Pair (!col, !rows);
	col := Int 0
    done;
    !rows

let encode_lm w =
  sim_val_tuple [Int w.fright; encode_pos w.lman.llocation; Int (dir_to_int w.lman.ldirection); Int w.lman.lives; Int w.lman.score]

let encode_ghs ghosts =
  let encode_gh ghost =
    sim_val_tuple [Int (int_of_gh_vitality ghost.gvitality); encode_pos ghost.glocation; Int (dir_to_int ghost.gdirection)] in
  sim_val_list (List.map encode_gh ghosts)

let encode_f fruit = Int fruit

let encode_world w =
  sim_val_tuple [encode_map w.map w.msize; encode_lm w; encode_ghs w.ghosts; encode_f w.fruit]

let check_mem m =
  if Stack.length m.data > 0 then
    failwith "program left something on the data stack"
  else if Stack.length m.control > 0 then
    failwith "program left something on the control stack"
  else ()

let init_lm_ai_mem world =
  let vals = Array.create 2 (Int 0) in
  vals.(0) <- encode_world world;
  let env = { parent = None; data = Some vals; size = 2 } in
    { pc = 0; data = Stack.create (); control = Stack.create (); env = env }

let run_ai_main mem prg =
  match Sim.run (Some mem) false prg with
  | (Sim.Pair (ai_init_state, Closure (ai_step, ai_step_frame)), m) ->
     check_mem mem; 
     (ai_init_state, ai_step, ai_step_frame, m)
  | _ -> failwith "program did not return a pair containing initial AI state and an AI step function"

let run_ai_step mem prg =
  match Sim.run (Some mem) false prg with
  | (Pair (new_ai_state, Int new_dir), m) ->
     check_mem mem;
     (new_ai_state, new_dir)
  | _ -> failwith "step function did not return a pair containing current AI state and new move"

let setup_env_frame m clo_entry clo_env ai_state world =
  let vals = Array.create 2 (Int 0) in
  let fp = { parent = Some clo_env; data = Some vals; size = 2 } in
  vals.(0) <- ai_state;
  vals.(1) <- encode_world world; 
  m.env <- fp;
  m.pc <- clo_entry

let setup_lambda_ai input world =
  let lexbuf = Lexing.from_channel input in
  let result = Parser.prg Lexer.token lexbuf in
  let mem = init_lm_ai_mem world in
  let prg = Array.of_list (Egcc.compile result) in
  let (initial_ai_state, ai_step, ai_step_frame, m) = run_ai_main mem prg in
  let curr_ai_state = ref initial_ai_state in
  let step_lambda_ai world =
    setup_env_frame mem ai_step ai_step_frame (!curr_ai_state) world;
    let (new_ai_state, new_dir) = run_ai_step mem prg in
      curr_ai_state := new_ai_state;
      Game.int_to_dir new_dir
  in step_lambda_ai

let setup_ghost_ai input ghost_index =
  let lexbuf = Lexing.from_channel input in
  let prg = Array.of_list (Eghc.compile (Eghcparser.prg Eghclexer.token lexbuf)) in
  let mem = Ghcsim.init_mem () in
  let ghost_ai_step world =
    mem.new_dir <- None;
    Ghcsim.run ghost_index false (Some mem) world prg;
    match mem.new_dir with
    | Some n -> Some (int_to_dir n)
    | None -> None
  in ghost_ai_step
