open Batteries
open Gamesim
open Game

let _ =
  let map_str = input_file Sys.argv.(3) in
  let world = fresh_world (build_map map_str) in
  let ghost_code = Common.cpp_string (input_file Sys.argv.(1)) in
  let lambda_code = input_file Sys.argv.(2) in
  let ghost_ai_step index = setup_ghost_ai (BatIO.input_string ghost_code) index in
  let lambda_lm_step = setup_lambda_ai (BatIO.input_string lambda_code) world in
    world.ghost_ais <- List.mapi (fun i _-> ghost_ai_step i) world.ghosts;
    world.lambda_ai <- lambda_lm_step;
    let debug = Array.length Sys.argv > 4 in
    let t = ref (if not debug then -1 else int_of_string Sys.argv.(4)) in
    while !t > world.utc && not world.game_over do
      if debug then
        print_world world;
      run_one_event world;
    done;
    Printf.printf "Game Over\n";
    print_world world

