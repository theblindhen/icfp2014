open Batteries
open Gamesim
open Game

let ghost_ais = ["default.eghc"; "four-weights.eghc"; "go-to-lambda.eghc"]
let lm_ais = ["go-right.egcc"; "semi-random-ai.egcc"; "strat_man.egcc"; "strat_man_no_fruit.egcc"; "strat_man_avoid1.egcc"; "strat_man_avoid50.egcc"; "strat_man_avoid_d6.egcc" ]
let maps = ["default"; "butterfly"; "bottleneck"; "straight-lines"; "maze"; "pill-sparse-maze"; "long-tunnels" ]

let run_till_end map ghost lman =
  let map_str = input_file ("maps/" ^ map) in
  let world = fresh_world (build_map map_str) in
  let ghost_code = Common.cpp_string (input_file ("ghc/ai/" ^ ghost)) in
  let lambda_code = input_file ("lambdaman/" ^ lman) in
  let ghost_ai_step index = setup_ghost_ai (BatIO.input_string ghost_code) index in
  let lambda_lm_step = setup_lambda_ai (BatIO.input_string lambda_code) world in
    world.ghost_ais <- List.mapi (fun i _-> ghost_ai_step i) world.ghosts;
    world.lambda_ai <- lambda_lm_step;
    while not world.game_over do
      run_one_event world;
    done;
    (world.lman.score, world.lman.lives)

let add_to_hashtbl tbl key x =
  match Hashtbl.find_option tbl key with
  | None ->
     Hashtbl.add tbl key x
  | Some y ->
     Hashtbl.replace tbl key (x + y)

let order_scores tbl =
  List.sort (fun (_, s1) (_, s2) -> s1 - s2)
    (Hashtbl.fold (fun key score acc -> (key, score)::acc) tbl [])

let print_results lm_scores gh_scores =
  let print_score (key, score) =
    Printf.printf "  %s - %d\n" key score in
  Printf.printf "Lambda AI scores (normalized):\n";
  List.iter print_score (List.rev (order_scores lm_scores));
  Printf.printf "Ghost AI scores (not normalized):\n";
  List.iter print_score (order_scores gh_scores)

let () =
  let lm_scores = Hashtbl.create 10 in
  let gh_scores = Hashtbl.create 10 in
  Printf.printf "Tournament starting\n";
  try
    List.iter (fun ghost ->
      List.iter (fun map ->
        Printf.printf "Map %s with ghost AI %s:\n" map ghost;
        let scores = Array.create (List.length lm_ais) (0, "") in
        List.iteri (fun idx lman ->
	  Printf.printf "  %s: " lman;
	  let (score, lives) = run_till_end map ghost lman in
	    Printf.printf "score = %d, lives = %d\n" score lives;
	    flush IO.stdout;
            scores.(idx) <- (score, lman);
	    add_to_hashtbl gh_scores ghost score) lm_ais;
        let scores =
          match Array.max scores with
          | (0, _) -> scores
          | (highest_score, _) -> Array.map (fun (score, lman) ->
                                               (1000*score / highest_score, lman)) scores
        in
        Array.iter (fun (score, lman) -> add_to_hashtbl lm_scores lman score) scores
      ) maps) ghost_ais;
    print_results lm_scores gh_scores
  with e ->
    Printf.printf "exception: %s\n" (Printexc.to_string e);
    print_results lm_scores gh_scores
    
