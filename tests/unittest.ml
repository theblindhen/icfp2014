open OUnit2

let () =
  run_test_tt_main ("Unit tests" >::: [
      Gcc.unittest ();
      Egcc.unittest ();
      Test_hlparser.unittest ();
      Sour.unittest ();
      Test_chain.unittest ();
      Test_hll_basics.unittest ();
      Test_hll_gamelogic.unittest ();
  ])
